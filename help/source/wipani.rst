.. index:: Wipani (Marketplace)

Wipani - The Marketplace
==========================

Wipani is the marketplace where you can access Processes and their related objectives, risks, controls and tests. This is a public repository for anyone to pull from. You may push your processes to Wipani, for others to use.

.. index:: Register and login

Register and Login
+++++++++++++++++++++

To use Wipani, you will have to register with your email address. Once you enter your email address and submit, an OTP will be sent to your email. Click on this link in the email to confirm your registration. 

Your email address will be used as your login and to track your process groups that you push or pull from Wipani.



.. index:: Pull Process Groups

Pull Process Groups
++++++++++++++++++++++

Once you have logged in, you will see the list of process groups that are available on Wipani. If you click on the name of the process group, you will be able to view the processes, objectives, risks, controls and tests. To pull, click on the "Pull" button on the right hand side.

.. index:: Push Process Groups

Push Process Groups
++++++++++++++++++++++

Once you have logged in, you will see the list of process groups that are available on Wipani. Next, click on repository and you will see the list of process groups that is in your system. Click on a process group name and you will see the list of processes. On the top of the process list, you will see a button "Push to Wipani". Click on this button. A warning message is displayed because you are about to push information to a public site. Once you confirm the push, the processes and all child objectives, risk, controls and tests will be copied to the Wipani.

You can see your process group in Wipani if you click on the Wipani menu.

