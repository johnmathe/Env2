# from builtins import str
import copy

from django.contrib import messages
from django.utils.translation import gettext
from elasticsearch.exceptions import ElasticsearchException

from search.es.index import index_document, index_delete


# from search.es.constants import doc_types

def indexer(data, module=None, doc_type=None, doc_id=None, request=None):
    if module == None:
        messages.error(request, gettext("Search indexing error."))
    else:
        index = module + "_" + doc_type
        # print(type(data))
        # print(vars(data))
        # for_index_data = vars(data)
        for_index_data = copy.deepcopy(vars(data))  # python create dict by reference https://thispointer.com/python-how-to-copy-a-dictionary-shallow-copy-vs-deep-copy/
        del for_index_data['_state']  # remove this object state key
        # print(for_index_data)
        # print(index)
        # print(doc_type)
        # print(doc_id)

        try:
            response = index_document(for_index_data, index, doc_type, doc_id, using='default', extra=None)
            if not response.get('success'):
                if request:
                    messages.error(request, gettext(doc_type + " could not be indexed."))
        except ElasticsearchException:
            if request:
                messages.error(request, gettext('Search indexing error while updating {' + doc_type + '}.'))
        else:
            pass


def delete_index(data, module=None, doc_type=None, doc_id=None, request=None):
    if module == None:
        messages.error(request, gettext("Search indexing error."))
    else:
        index = module + "_" + doc_type
        # print(type(data))
        # print(vars(data))
        # for_index_data = vars(data)
        for_index_data = copy.deepcopy(vars(data))  # python create dict by reference https://thispointer.com/python-how-to-copy-a-dictionary-shallow-copy-vs-deep-copy/
        del for_index_data['_state']  # remove this object state key
        # print(for_index_data)
        # print(index)
        # print(doc_type)
        # print(doc_id)

        try:
            response = index_delete(for_index_data, index, doc_type, doc_id, using='default', extra=None)
            print(response)
            if not response.get('success'):
                if request:
                    messages.error(request, gettext(doc_type + " could not be removed from search index."))
        except ElasticsearchException:
            if request:
                messages.error(request, gettext('Search indexing error while removing {' + doc_type + '}.'))
        else:
            pass
