from django import forms
from django.contrib.auth.models import User
from django.utils.translation import gettext

from repository.models import Process_Group, Process, Objective, Risk, Control, Test


class WipaniRegisterForm(forms.ModelForm):
    email = forms.EmailField(
        label='Email',
        required=True,
    )
    first_name = forms.CharField(
        label="First Name",
        required=True,
    )
    last_name = forms.CharField(
        label="Last Name",
        required=True,
    )

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


class WipaniTokenForm(forms.Form):
    email = forms.EmailField(
        label="Email",
        required=False,
        widget=forms.EmailInput(attrs={
            'autocapitalize': 'none',
            'autocomplete': 'email',
            'autofocus': True,
            'style': 'width: 300px; padding-left: 8px;'
        }),
    )
    token = forms.CharField(
        label="Authentication Token",
        required=False,
        min_length=40,
        max_length=40,
        widget=forms.TextInput(attrs={
            'style': 'width: 333px; padding-left: 8px;'
            }),
    )


class ProcessGroupForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProcessGroupForm, self).__init__(*args, **kwargs)
        for field_name, field in list(self.fields.items()):
            field.widget.attrs['class'] = 'form-control'
        # if self.instance:
        # self.fields['belong_to'].queryset = Process_Group.objects.all()\
        # .exclude(id=self.instance.id)
        # self.fields['title'].label = gettext("Title")
        # self.fields['desc'].label = gettext("Description")

    class Meta:
        model = Process_Group
        fields = ('title', 'desc')


class AjaxProcessGroupForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        # pgs = kwargs.pop('pgs', None)
        super(AjaxProcessGroupForm, self).__init__(*args, **kwargs)
        # self.fields['title'].label = gettext("Title")
        # self.fields['desc'].label = gettext("Description")
        # self.fields['belong_to'].label = gettext("Belongs to")
        for field_name, field in list(self.fields.items()):
            field.widget.attrs['class'] = 'form-control'
        if self.instance:
            self.fields['belong_to'].queryset = Process_Group.objects \
                .exclude(id=self.instance.id)
        else:
            pgs = Process.objects.values_list('pg', flat=True)
            self.fields['belong_to'].queryset = Process_Group.objects \
                .filter(id__in=pgs)
        # if self.instance:
        # pgs = list(set(pgs) - set([self.instance.id]))
        # self.fields['belong_to'].queryset = Process_Group.objects\
        # .filter(id__in=pgs)
        # else:
        # self.fields['belong_to'].queryset = Process_Group.objects\
        # .filter(id__in=pgs)

    class Meta:
        model = Process_Group
        fields = ('title', 'desc', 'belong_to')


class AjaxProcessForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        # pgs = kwargs.pop('pgs', None)
        super(AjaxProcessForm, self).__init__(*args, **kwargs)
        for field_name, field in list(self.fields.items()):
            field.widget.attrs['class'] = 'form-control'
        # self.fields['pg'].queryset = Process_Group.objects\
        # .filter(id__in=pgs)

    class Meta:
        model = Process
        fields = ('title', 'ref_id', 'desc', 'owner')
        error_messages = {
            'ref_id': {
                'required': gettext("A reference is required!"),
            },
        }


class AjaxObjectiveForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        pg = kwargs.pop('pg', None)
        super(AjaxObjectiveForm, self).__init__(*args, **kwargs)
        # self.fields['title'].label = gettext("Title")
        # self.fields['ref_id'].label = gettext("Reference ID")
        # self.fields['desc'].label = gettext("Description")
        # self.fields['process'].label = gettext("Process")

        for field_name, field in list(self.fields.items()):
            field.widget.attrs['class'] = 'form-control'
        self.fields['process'].queryset = pg.process_group.all()

    class Meta:
        model = Objective
        fields = ('title', 'ref_id', 'desc', 'process')
        error_messages = {
            'ref_id': {
                'required': gettext("A reference is required!"),
            },
            'process': {
                'required': gettext("Please select a parent Process."),
            },
        }


class AjaxRiskForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        pg = kwargs.pop('pg', None)
        super(AjaxRiskForm, self).__init__(*args, **kwargs)
        # self.fields['title'].label = gettext("Title")
        # self.fields['ref_id'].label = gettext("Reference ID")
        # self.fields['desc'].label = gettext("Description")
        # self.fields['objective'].label = gettext("Objective")
        # self.fields['treatment'].label = gettext("Treatment")
        for field_name, field in list(self.fields.items()):
            field.widget.attrs['class'] = 'form-control'
        self.fields['objective'].queryset = pg.process_group_objective.all()

    class Meta:
        model = Risk
        fields = ('title', 'ref_id', 'desc', 'objective', 'treatment')
        error_messages = {
            'ref_id': {
                'required': gettext("A reference is required!"),
            },
            'objective': {
                'required': gettext("Please select a parent Objective."),
            },
        }


class AjaxControlForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        pg = kwargs.pop('pg', None)
        super(AjaxControlForm, self).__init__(*args, **kwargs)
        # self.fields['title'].label = gettext("Title")
        # self.fields['ref_id'].label = gettext("Reference ID")
        # self.fields['desc'].label = gettext("Description")
        # self.fields['control_type'].label = gettext("Control Type")
        # self.fields['risk'].label = gettext("Risk")
        # self.fields['owner'].label = gettext("Control Owner")
        # self.fields['control_class'].label = gettext("Control Class")
        # self.fields['frequency'].label = gettext("Frequency")

        for field_name, field in list(self.fields.items()):
            field.widget.attrs['class'] = 'form-control'
        self.fields['risk'].queryset = pg.process_group_risk.all()

    class Meta:
        model = Control
        fields = ('title', 'ref_id', 'desc', 'control_type', 'risk', 'owner',
                  'control_class', 'frequency')
        error_messages = {
            'ref_id': {
                'required': gettext("A reference is required!"),
            },
            'risk': {
                'required': gettext("Please select a parent Risk."),
            },
        }


class AjaxTestForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        pg = kwargs.pop('pg', None)
        super(AjaxTestForm, self).__init__(*args, **kwargs)
        # self.fields['title'].label = gettext("Title")
        # self.fields['ref_id'].label = gettext("Reference ID")
        # self.fields['desc'].label = gettext("Description")
        # self.fields['control'].label = gettext("Control")

        for field_name, field in list(self.fields.items()):
            field.widget.attrs['class'] = 'form-control'
        self.fields['control'].queryset = pg.process_group_control.all()

    class Meta:
        model = Test
        fields = ('title', 'ref_id', 'desc', 'control')
        error_messages = {
            'ref_id': {
                'required': gettext("A reference is required!"),
            },
            'control': {
                'required': gettext("Please select a parent Control."),
            },
        }
