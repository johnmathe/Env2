from django.contrib.auth.mixins import AccessMixin
from django.shortcuts import redirect
from django.urls import reverse

from repository.models import WipaniToken


class IsWipaniAuthenticated(AccessMixin):

    def __init__(self):
        self.request = None
        self.token = None

    def dispatch(self, request, *args, **kwargs):
        if not request.is_wipani_authenticated:
            return redirect(reverse('repo:WipaniToken')+'?next='+request.path_info)
        self.token = WipaniToken.objects.all().first().token
        return super(IsWipaniAuthenticated, self).dispatch(request, *args, **kwargs)
