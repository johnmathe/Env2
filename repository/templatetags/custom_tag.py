from django import template
from django.template.defaultfilters import stringfilter
import re

register = template.Library()


@register.filter(is_safe=True)
@stringfilter
def make_readable(value):
    t = re.sub("([a-z])'([A-Z])", lambda m: m[0].lower(), value.title())
    capped = re.sub(r'\d([A-Z])', lambda m: m[0].lower(), t)
    return capped.replace('_', ' ')
