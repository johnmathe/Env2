from rest_framework import serializers
from repository.models import Process_Group, Process, Test, Control, Risk, Objective


class ProcessGroupSerializer(serializers.ModelSerializer):
    changed_on = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Process_Group
        fields = ['id', 'title', 'desc', 'changed_on']


class ProcessSerializer(serializers.ModelSerializer):
    changed_on = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Process
        fields = ['id', 'title', 'ref_id', 'desc', 'changed_on']
        depth = 1


class ObjectiveSerializer(serializers.ModelSerializer):
    process = ProcessSerializer(required=False, many=True, read_only=True)
    changed_on = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Objective
        fields = ['id', 'process', 'title', 'ref_id', 'desc', 'changed_on']
        depth = 1


class RiskSerializer(serializers.ModelSerializer):
    objective = ObjectiveSerializer(required=False, many=True, read_only=True)
    changed_on = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Risk
        fields = ['id', 'objective', 'title', 'ref_id', 'desc', 'treatment', 'changed_on']
        depth = 1


class ControlSerializer(serializers.ModelSerializer):
    risk = RiskSerializer(required=False, many=True, read_only=True)
    changed_on = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Control
        fields = ['id', 'risk', 'title', 'ref_id', 'desc', 'control_type', 'control_class', 'frequency', 'changed_on']
        depth = 1


class TestSerializer(serializers.ModelSerializer):
    control = ControlSerializer(required=False, many=True, read_only=True)
    changed_on = serializers.DateTimeField(read_only=True)

    class Meta:
        model = Test
        fields = ['id', 'control', 'title', 'ref_id', 'desc', 'changed_on']
        depth = 1
