# -*- coding: utf-8 -*-
from django.template.loader import render_to_string

from repository.models import Process, Process_Group, Objective, \
    Risk, Control, Test


def get_pg_childs(ctype, id):
    result = []
    if ctype == 'process_group' and id is None:
        process_groups = Process_Group.objects \
            .filter(belong_to__isnull=True) \
            .extra(select={'class_name': "'process_group'"}).values('id',
                                                                    'title', 'class_name').order_by("id").distinct()
        result = process_groups
    elif ctype == 'process_group':
        process_groups = Process_Group.objects \
            .filter(belong_to_id=id) \
            .extra(select={'class_name': "'process_group'"}).values('id',
                                                                    'title', 'class_name').order_by("id").distinct()
        process = Process.objects \
            .filter(pg_id=id) \
            .extra(select={'class_name': "'process'"}).values('id',
                                                              'title', 'class_name').order_by("id").distinct()
        result = list(process_groups) + list(process)
    elif ctype == 'process':
        result = Objective.objects.filter(process__in=[id]) \
            .extra(select={'class_name': "'objective'"}).values('id',
                                                                'title', 'class_name').order_by("id").distinct()
    elif ctype == 'objective':
        result = Risk.objects.filter(objective__in=[id]) \
            .extra(select={'class_name': "'risk'"}).values('id',
                                                           'title', 'class_name').order_by("id").distinct()
    elif ctype == 'risk':
        result = Control.objects.filter(risk__in=[id]) \
            .extra(select={'class_name': "'control'"}).values('id',
                                                              'title', 'class_name').order_by("id").distinct()
    elif ctype == 'control':
        result = Test.objects.filter(control__in=[id]) \
            .extra(select={'class_name': "'test'"}).values('id',
                                                           'title', 'class_name').order_by("id").distinct()
    return result


def get_process_generated_id(generated_id, pg, process):
    parent_pg = process.pg
    while (not parent_pg.id == pg.id):
        generated_id += "process_group_%s" % (process.pg_id)
        parent_pg = parent_pg.belong_to
    return generated_id


def get_pg_binding_html(ctype, obj):
    data = {}
    generated_id = "pg_0"
    if ctype == 'process_group':
        gen_id = ''
        parent_pg = obj.belong_to
        while (parent_pg is not None):
            if gen_id != '':
                gen_id = "process_group_%s_" % (parent_pg.id) + gen_id
            else:
                gen_id = "process_group_%s" % (parent_pg.id)
            parent_pg = parent_pg.belong_to
        generated_id = generated_id + '_' + gen_id
        data['acc_%s' % (generated_id)] = \
            render_to_string('repo/management/ajax_binding_child.html',
                             {'class_name': ctype, 'obj': obj,
                              'generated_id': generated_id})
    return data


def get_binding_html(pg, ctype, obj):
    data = {}
    generated_id = "pg_%s" % (pg.id)
    if ctype == 'process_group':
        parent_pg = obj.belong_to
        while (not parent_pg.id == pg.id):
            generated_id += "process_group_%s" % (parent_pg.id)
            parent_pg = parent_pg.belong_to
        data['acc_%s' % (generated_id)] = \
            render_to_string('repo/management/ajax_binding_child.html',
                             {'class_name': ctype, 'obj': obj,
                              'generated_id': generated_id})
    elif ctype == 'process':
        new_generated_id = get_process_generated_id(generated_id, pg, obj)
        data['acc_%s' % (new_generated_id)] = \
            render_to_string('repo/management/ajax_binding_child.html',
                             {'class_name': ctype, 'obj': obj,
                              'generated_id': new_generated_id})
    elif ctype == 'objective':
        for process in obj.process.all():
            process_gen_id = get_process_generated_id(generated_id, pg, process)
            new_generated_id = "%s_process_%s" % (process_gen_id, process.id)
            data['acc_%s' % (new_generated_id)] = \
                render_to_string('repo/management/ajax_binding_child.html',
                                 {'class_name': ctype, 'obj': obj,
                                  'generated_id': new_generated_id})
    elif ctype == 'risk':
        for objective in obj.objective.all():
            for process in objective.process.all():
                process_gen_id = get_process_generated_id(generated_id,
                                                          pg, process)
                new_generated_id = \
                    "%s_process_%s_objective_%s" % (process_gen_id,
                                                    process.id, objective.id)
                data['acc_%s' % (new_generated_id)] = \
                    render_to_string('repo/management/ajax_binding_child.html',
                                     {'class_name': ctype, 'obj': obj,
                                      'generated_id': new_generated_id})
    elif ctype == 'control':
        for risk in obj.risk.all():
            for objective in risk.objective.all():
                for process in objective.process.all():
                    process_gen_id = get_process_generated_id(generated_id,
                                                              pg, process)
                    new_generated_id = \
                        "%s_process_%s_objective_%s_risk_%s" % (process_gen_id,
                                                                process.id, objective.id, risk.id)
                    data['acc_%s' % (new_generated_id)] = \
                        render_to_string('repo/management/ajax_binding_child.html',
                                         {'class_name': ctype, 'obj': obj,
                                          'generated_id': new_generated_id})
    elif ctype == 'test':
        for control in obj.control.all():
            for risk in control.risk.all():
                for objective in risk.objective.all():
                    for process in objective.process.all():
                        process_gen_id = get_process_generated_id(generated_id,
                                                                  pg, process)
                        new_generated_id = \
                            "%s_process_%s_objective_%s_risk_%s_control_%s" \
                            % (process_gen_id, process.id, objective.id,
                               risk.id, control.id)
                        data['acc_%s' % (new_generated_id)] = \
                            render_to_string('repo/management/ajax_binding_child.html',
                                             {'class_name': ctype, 'obj': obj,
                                              'generated_id': new_generated_id})
    return data
