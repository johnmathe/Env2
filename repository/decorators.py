# -*- coding: utf-8 -*-
from django.core.exceptions import PermissionDenied


def repository_view_permission_required(view_func):
    def _wrapped_view_func(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            if kwargs['ctype'] == 'process_group' \
                    and not request.user.has_perm('repository.view_process_group'):
                raise PermissionDenied
            elif kwargs['ctype'] == 'process' \
                    and not request.user.has_perm('repository.view_process'):
                raise PermissionDenied
            elif kwargs['ctype'] == 'objective' \
                    and not request.user.has_perm('repository.view_objective'):
                raise PermissionDenied
            elif kwargs['ctype'] == 'risk' \
                    and not request.user.has_perm('repository.view_risk'):
                raise PermissionDenied
            elif kwargs['ctype'] == 'control' \
                    and not request.user.has_perm('repository.view_control'):
                raise PermissionDenied
            elif kwargs['ctype'] == 'test' \
                    and not request.user.has_perm('repository.view_test'):
                raise PermissionDenied
        return view_func(self, request, *args, **kwargs)

    return _wrapped_view_func


def repository_change_permission_required(view_func):
    def _wrapped_view_func(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            if kwargs['ctype'] == 'process_group' \
                    and not request.user.has_perm('repository.change_process_group'):
                raise PermissionDenied
            elif kwargs['ctype'] == 'process' \
                    and not request.user.has_perm('repository.change_process'):
                raise PermissionDenied
            elif kwargs['ctype'] == 'objective' \
                    and not request.user.has_perm('repository.change_objective'):
                raise PermissionDenied
            elif kwargs['ctype'] == 'risk' \
                    and not request.user.has_perm('repository.change_risk'):
                raise PermissionDenied
            elif kwargs['ctype'] == 'control' \
                    and not request.user.has_perm('repository.change_control'):
                raise PermissionDenied
            elif kwargs['ctype'] == 'test' \
                    and not request.user.has_perm('repository.change_test'):
                raise PermissionDenied
        return view_func(self, request, *args, **kwargs)

    return _wrapped_view_func
