import logging
from django.contrib import messages
from django.contrib.auth.decorators import permission_required
from django.http import JsonResponse
from django.http.response import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.utils.translation import gettext
from django.views import generic
from django.views.decorators.csrf import csrf_exempt

from repository.decorators import repository_view_permission_required, \
    repository_change_permission_required
from repository.forms import ProcessGroupForm, AjaxProcessForm, \
    AjaxObjectiveForm, AjaxRiskForm, AjaxControlForm, AjaxTestForm, \
    AjaxProcessGroupForm
from repository.helpers import get_pg_childs, get_binding_html, \
    get_pg_binding_html
from repository.models import Process_Group, Process, Objective, \
    Risk, Control, Test, Attachment

# from repository.search_index import indexer

# TODO: move paginate to core
PAGINATE = 5

# Get an instance of a logger
logger = logging.getLogger(__name__)

"""
@method_decorator(permission_required('repository.view_process_group'),
     name='dispatch')
class ReposIndexView(generic.ListView):
    template_name = 'repo/repo_index.html'
    context_object_name = 'all_repos'

    def get_queryset(self):
        return Process_Group.objects.filter(belong_to__isnull=True)\
        .order_by('id')
"""


class ReposIndexView(generic.ListView):
    template_name = 'repo/pg_index.html'
    paginate_by = PAGINATE

    def get_queryset(self):
        return get_pg_childs('process_group', None)

    def get_context_data(self, **kwargs):
        context = super(ReposIndexView, self).get_context_data(**kwargs)
        context['process_group_id'] = self.kwargs.get('process_group_id', 0)
        context['management'] = True
        context['generated_id'] = 'pg_%s' % (self.kwargs.get('process_group_id', 0))
        # self.request.session['pg_childs'] = pg.get_child_list()
        # self.request.session['pg_parents'] = pg.get_all_process_groups()
        # list(set(pg.get_all_process_groups()) - set([pg.id]))
        # print(">>>>M>M>MM>M>M>M>M>M>", self.request.session['pg_childs'])
        # print(">>>>PGSSSS", self.request.session['pg_parents'])
        return context


@method_decorator(permission_required('repository.change_process_group'),
                  name='get')
@method_decorator(permission_required('repository.change_process_group'),
                  name='post')
class ProcessGroupCreateView(generic.View):

    def get(self, request):
        form = ProcessGroupForm()
        return render(request, "repo/process_group.html", {'form': form})

    def post(self, request):
        form = ProcessGroupForm(request.POST)
        if form.is_valid():
            pg = form.save(commit=False)
            pg.changed_by = request.user
            pg.changed_on = timezone.now()
            pg.save()
            # search
            # indexer(pg, "repository", "process_group", pg.id, request)
            # try:
            #     response = index_document(pg, "repository", "process_group", pg.id)
            #     # print(response)
            #     if not response.get('success'):
            #         messages.error(request, gettext("Process Group could not be indexed."))
            #         logger.warning("ProcessGroupCreateView: " + response.get('message'))
            # except ElasticsearchException as e:
            #     messages.error(request, gettext('Search Error while indexing Process Group'))
            #     logger.error('ProcessGroupCreateView: ' + str(e))

            messages.success(request, gettext('Process Group was saved'))
            if request.POST.get('add_manually') == "":
                messages.success(request, "add manually")
                return HttpResponseRedirect(reverse('repo:pg_management',
                                                    args=[pg.id]))

            else:
                messages.success(request, gettext("Process Group was created"))
                return HttpResponseRedirect(reverse('repo:process_group',
                                                    args=(pg.id,)))

        else:
            messages.error(request, gettext('Your process group was NOT saved'))
            return render(request, "repo/process_group.html", {'form': form})


@method_decorator(permission_required('repository.change_process_group'),
                  name='get')
@method_decorator(permission_required('repository.change_process_group'),
                  name='post')
class ProcessGroupDetailView(generic.View):

    def get(self, request, process_group_id):
        pg = get_object_or_404(Process_Group, pk=process_group_id)
        form = ProcessGroupForm(instance=pg)
        return render(request, "repo/process_group.html", {'form': form})

    def post(self, request, process_group_id):
        pg = get_object_or_404(Process_Group, pk=process_group_id)
        if request.method == "POST":
            form = ProcessGroupForm(request.POST, request.FILES, instance=pg)

            if form.is_valid():
                pg = form.save(commit=False)
                pg.changed_by = request.user
                pg.changed_on = timezone.now()
                pg = form.save()
                messages.success(request, gettext('Process group information was saved'))
                if request.POST.get('add_manually') == "":
                    messages.info(request, gettext('You may now add processes'))
                    return HttpResponseRedirect(reverse('repo:pg_management', args=[pg.id]))
                elif request.POST.get('delete') == "":
                    # try:
                    #     response = index_delete(pg, "repository", "process_group", pg.id)
                    #     # print(response)
                    #     if not response.get('success'):
                    #         messages.error(request, gettext("Search indexing error while deleting the Process Group."))
                    #         logger.warning("ProcessGroupDetailView: " + response.get('message'))
                    # except Exception as e:
                    #     messages.error(request, gettext('Process Group could not be deleted from Search index.'))
                    #     logger.error('ProcessGroupDetailView: ' + str(e))
                    # else:
                    pg.delete()
                    messages.success(request, gettext('Process Group is deleted.'))
                    return HttpResponseRedirect(reverse('repo:repo_index'))
                else:
                    return HttpResponseRedirect(reverse('repo:process_group', args=(pg.id,)))

            else:
                messages.error(request, gettext('Your process group was NOT saved'))
                return render(request, "repo/process_group.html", {'form': form})


@method_decorator(permission_required('repository.change_process_group'), name='dispatch')
class ProcessGroupManagement(generic.ListView):
    template_name = 'repo/management.html'
    paginate_by = PAGINATE

    def get_queryset(self):
        return get_pg_childs('process_group', self.kwargs['process_group_id'])

    def get_context_data(self, **kwargs):
        context = super(ProcessGroupManagement, self).get_context_data(**kwargs)
        context['process_group_id'] = self.kwargs['process_group_id']
        context['pg'] = pg = Process_Group.objects.get(id=self.kwargs['process_group_id'])
        context['management'] = True
        context['generated_id'] = 'pg_%s' % (self.kwargs['process_group_id'])
        breadcrumb = [pg.title]
        while pg.belong_to:
            breadcrumb.append(pg.belong_to.title)
            pg = pg.belong_to
        context['breadcrumb'] = breadcrumb[::-1]
        # self.request.session['pg_childs'] = pg.get_child_list()
        # self.request.session['pg_parents'] = pg.get_all_process_groups()
        # list(set(pg.get_all_process_groups()) - set([pg.id]))
        return context


class ProcessGroupGetChilds(generic.View):
    @csrf_exempt
    @repository_view_permission_required
    def get(self, request, process_group_id, ctype, object_id):
        data = {}
        object_list = get_pg_childs(ctype, object_id)
        generated_id = request.GET.get('generated_id')
        copy = request.GET.get('copy')
        pg_index = request.GET.get('pg_index')
        if copy:
            template = "repo/copy/childs.html"
        elif pg_index:
            template = "repo/pg_childs.html"
        else:
            template = "repo/childs.html"
        data['html'] = render_to_string(template,
                                        {'generated_id': generated_id, 'object_list': object_list})
        return JsonResponse(data)


class AjaxChildsAddEditView(generic.View):
    def _get_model(self, ctype):
        model_map = {'process_group': Process_Group, 'process': Process,
                     'objective': Objective, 'risk': Risk,
                     'control': Control, 'test': Test}
        return model_map[ctype]

    def _get_form(self, ctype):
        form_map = {'process_group': AjaxProcessGroupForm,
                    'process': AjaxProcessForm,
                    'objective': AjaxObjectiveForm, 'risk': AjaxRiskForm,
                    'control': AjaxControlForm, 'test': AjaxTestForm}
        return form_map[ctype]

    @csrf_exempt
    @repository_change_permission_required
    def get(self, request, process_group_id, ctype, object_id=None):
        data = {}
        form = self._get_form(ctype)
        if process_group_id != 0:
            pg = get_object_or_404(Process_Group, pk=process_group_id)
        # print(ctype)
        if object_id:
            # print(object_id)
            instance = get_object_or_404(self._get_model(ctype), pk=object_id)
            if ctype in ['objective', 'risk', 'control', 'test']:
                form = form(instance=instance, pg=pg)
            # else:
            # form = form(instance=instance)
            elif ctype == 'process':
                form = form(instance=instance)
            elif ctype == 'process_group':
                form = form(instance=instance)
        else:
            if ctype in ['objective', 'risk', 'control', 'test']:
                form = form(pg=pg)
            elif ctype == 'process':
                # form = form(pgs=self.request.session['pg_childs'])
                form = form()
            elif ctype == 'process_group':
                form = form()
        csrf_token_value = request.COOKIES['csrftoken']
        data['html'] = render_to_string('repo/management/right_panel_%s.html' % (ctype),
                             {'form': form, 'csrf_token_value': csrf_token_value})
        return JsonResponse(data)

    @csrf_exempt
    @repository_change_permission_required
    def post(self, request, process_group_id, ctype, object_id=None):
        data = {'status': 'failed'}
        form = self._get_form(ctype)
        instance = None
        if process_group_id != 0:
            pg = get_object_or_404(Process_Group, pk=process_group_id)
        else:
            pg = None
        if object_id:
            instance = get_object_or_404(self._get_model(ctype), pk=object_id)
            # if ctype in ['objective', 'risk', 'control', 'test']:
            # form = form(request.POST, instance=instance, pg=pg)
            # else:
            # form = form(request.POST, instance=instance)
            if ctype in ['objective', 'risk', 'control', 'test']:
                form = form(request.POST, instance=instance, pg=pg)
            elif ctype == 'process':
                form = form(request.POST, instance=instance)
            elif ctype == 'process_group':
                form = form(request.POST, instance=instance)
        else:
            if ctype in ['objective', 'risk', 'control', 'test']:
                form = form(request.POST, pg=pg)
            # else:
            # form = form(request.POST)
            elif ctype == 'process':
                form = form(request.POST)
            elif ctype == 'process_group':
                form = form(request.POST)
        if form.is_valid():
            # if instance:
            # if ctype == 'process_group':
            # old_belong_to = instance.belong_to.id
            # elif ctype == 'process':
            # old_pg = instance.pg.id
            obj = form.save(commit=False)
            if not object_id:
                obj.changed_by = request.user
                # if ctype == 'process_group':
                # obj.belong_to = pg
                # else:
                # obj.pg = pg
                if not ctype == 'process_group':
                    obj.pg = pg
            obj.save()
            # indexer(obj, "repository", ctype, obj.id, request)
            # try:
            #     if ctype == 'objective':
            #         response = index_document(obj, "repository", ctype, obj.id, exclude=['process'])
            #     elif ctype == 'risk':
            #         response = index_document(obj, "repository", ctype, obj.id, exclude=['objective'])
            #     elif ctype == 'control':
            #         response = index_document(obj, "repository", ctype, obj.id, exclude=['risk'])
            #     elif ctype == 'test':
            #         response = index_document(obj, "repository", ctype, obj.id, exclude=['control'])
            #     else:
            #         response = index_document(obj, "repository", ctype, obj.id)
            #     # print(response)
            #     if not response.get('success'):
            #         messages.error(request, gettext(ctype + " could not be indexed."))
            #         logger.warning("Repo-AjaxChildsAddEditView: " + response.get('message'))
            # except ElasticsearchException as e:
            #     messages.error(request, gettext('Search Error while indexing ' + ctype))
            #     logger.error('Repo-AjaxChildsAddEditView: ' + str(e))

            form.save_m2m()
            """
            if ctype == 'process_group':
                if instance is None:
                    self.request.session['pg_parents'] += [obj.id]
                    self.request.session['pg_childs'] += [obj.id]
                #elif obj.belong_to_id in self.request.session['pg_childs']:
                if obj.belong_to_id in self.request.session['pg_childs']:
                    self.request.session['pg_childs'] =\
                     list(set(self.request.session['pg_childs']) - set([obj.belong_to_id]))

            elif ctype == 'process':
                if (instance and old_pg != obj.pg_id) or instance is None:
                    #self.request.session['pg_childs'] =\
                     #list(set(self.request.session['pg_childs']) - set([obj.pg_id]))
                    self.request.session['pg_parents'] =\
                     list(set(self.request.session['pg_parents']) - set([obj.pg_id]))
                if instance:
                    self.request.session['pg_childs'].append(old_pg)
            """
            try:
                papers = request.FILES.getlist('papers')
                if papers:
                    Attachment.objects.bulk_create(
                        [Attachment(content_object=obj,
                                    workpaper=attachment, filename=attachment.name)
                         for attachment in papers])
            except:
                pass
            if pg:
                data['html'] = get_binding_html(pg, ctype, obj)
            else:
                data['html'] = get_pg_binding_html(ctype, obj)
            data['title'] = obj.title
            data['status'] = 'success'
        else:
            data['error'] = form.errors
        return JsonResponse(data)


class AjaxAttachmentDeleteView(generic.View):
    @csrf_exempt
    def get(self, request, attachment_id):
        data = {}
        Attachment.objects.get(id=attachment_id).delete()
        data['status'] = 'success'
        return JsonResponse(data)


class AjaxChildsDeleteView(generic.View):
    def _get_model(self, ctype):
        model_map = {'process_group': Process_Group, 'process': Process,
                     'objective': Objective, 'risk': Risk,
                     'control': Control, 'test': Test}
        return model_map[ctype]

    def delete_childs(self, instance):
        if isinstance(instance, Test):
            instance.delete()
        else:
            childs = list(instance.get_childs())
            instance.delete()
            for child in childs:
                if not child.get_parents().exists():
                    self.delete_childs(child)

    @csrf_exempt
    @repository_change_permission_required
    def get(self, request, process_group_id, ctype, object_id):
        data = {}
        child = get_object_or_404(self._get_model(ctype), id=object_id)
        # search
        # try:
        #     response = index_delete(child, "repository", ctype, object_id)
        #     # print(response)
        #     if not response.get('success'):
        #         messages.error(request, gettext("Search indexing error while deleting the " + ctype))
        #         logger.warning("Repo-AjaxChildsDeleteView: " + response.get('message'))
        # except Exception as e:
        #     messages.error(request, gettext(ctype + " could not be deleted from Search index."))
        #     logger.error('Repo-AjaxChildsDeleteView: ' + str(e))
        #     data['status'] = 'fail'
        # else:
        # get_object_or_404(self._get_model(ctype), id=object_id).delete()

        if isinstance(child, Process_Group):
            Process.objects.filter(pg=child).delete()
            Objective.objects.filter(pg=child).delete()
            Risk.objects.filter(pg=child).delete()
            Control.objects.filter(pg=child).delete()
            Test.objects.filter(pg=child).delete()
            child.delete()
        else:
            self.delete_childs(child)

        data['status'] = 'success'

        return JsonResponse(data)


class RepoCopyView(generic.ListView):
    template_name = 'repo/copy/repo_copy.html'
    paginate_by = PAGINATE

    def get_queryset(self):
        return Process_Group.objects.filter(belong_to__isnull=True) \
            .extra(select={'class_name': "'process_group'"}).values('id',
                                                                    'title', 'class_name').distinct()

    def get_context_data(self, **kwargs):
        context = super(RepoCopyView, self).get_context_data(**kwargs)
        context['process_group_id'] = 0
        context['generated_id'] = 'pg_0'
        context['audit_id'] = self.kwargs['audit_id']
        return context
