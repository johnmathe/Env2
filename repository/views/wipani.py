import json
import requests

from django.contrib import messages
from django.db import transaction
from django.http import JsonResponse, Http404
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.urls import reverse, reverse_lazy
from django.utils.safestring import mark_safe
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from repository.forms import WipaniRegisterForm, WipaniTokenForm
from repository.mixins import IsWipaniAuthenticated
from repository.models import Process_Group, WipaniToken
from repository.serializers import ProcessGroupSerializer, ObjectiveSerializer, RiskSerializer, ProcessSerializer, ControlSerializer, TestSerializer


def validateJSON(jsonData):
    try:
        json.loads(jsonData)
    except ValueError:
        return False
    return True


class RegisterView(generic.FormView):
    template_name = 'repo/wipani/register.html'
    form_class = WipaniRegisterForm
    success_url = reverse_lazy('repo:WipaniToken')

    def form_valid(self, form):
        try:
            result = requests.post(settings.WIPANI_URL + 'register/', form.cleaned_data)
        except (requests.exceptions.ConnectionError, requests.exceptions.ConnectTimeout):
            messages.error(self.request, 'Connection cannot be established to Wipani! Please try again later.')
            return self.render_to_response(self.get_context_data(form=form))
        except Exception as e:
            messages.error(self.request, e)
            return self.render_to_response(self.get_context_data(form=form))
        if result.status_code == 201:
            messages.success(self.request, "Account Created. Email sent to " + form.cleaned_data.get('email'))
            return super().form_valid(form)
        else:
            if result.status_code == 400:
                for key in result.json().keys():
                    if key == "non_field_errors":
                        form.add_error(field=None, error=result.json()[key])
                    else:
                        form.add_error(field=key, error=result.json()[key])
                return super().form_invalid(form)
            if result.status_code == 404:
                messages.error(self.request, 'Connection cannot be established to Wipani! [404]')
                return self.render_to_response(self.get_context_data(form=form))
            if validateJSON(result.content):
                error = ''
                for key in result.json().keys():
                    error = error + '<br/>' + result.json()[key]
                messages.error(self.request, mark_safe('Wipani Error [' + str(result.status_code) + ']:' + error))
            else:
                messages.error(self.request, 'Wipani Error [' + str(result.status_code) + ']')
            return self.render_to_response(self.get_context_data(form=form))


class TokenView(generic.FormView):
    template_name = 'repo/wipani/token.html'
    form_class = WipaniTokenForm
    success_url = reverse_lazy('repo:WipaniList')

    def get_success_url(self):
        success_url = self.request.GET.get('next', str(self.success_url))
        return success_url

    def get_initial(self):
        initial = super().get_initial()
        if self.request.is_wipani_authenticated:
            initial['token'] = WipaniToken.objects.all().first().token
        return initial

    def form_valid(self, form):
        if 'save' in self.request.POST:
            if(form.cleaned_data.get('token')):
                if len(form.cleaned_data.get('token')) != 40:
                    form.add_error(field='token', error="Invalid Token! Token should have a length of 40 characters!")
                else:
                    if self.request.is_wipani_authenticated:
                        wipani_token = WipaniToken.objects.all().first()
                        wipani_token.token = form.cleaned_data.get('token')
                        wipani_token.save()
                        messages.success(self.request, "Authentication Token Updated!")
                    else:
                        WipaniToken.objects.create(token=form.cleaned_data.get('token'))
                        messages.success(self.request, "Logged in to Wipani!")
                    return super().form_valid(form)
            else:
                form.add_error(field='token', error='Token is required!')
        elif 'get' in self.request.POST:
            if(form.cleaned_data.get('email')):
                try:
                    result = requests.post(settings.WIPANI_URL + 'token/', form.cleaned_data)
                except (requests.exceptions.ConnectionError, requests.exceptions.ConnectTimeout):
                    messages.error(self.request, 'Connection cannot be established to Wipani! Please try again later.')
                    return self.render_to_response(self.get_context_data(form=form))
                except Exception as e:
                    messages.error(self.request, e)
                    return self.render_to_response(self.get_context_data(form=form))
                if result.status_code == 200:
                    messages.success(self.request, f"Email sent to {form.cleaned_data.get('email')}")
                else:
                    if result.status_code == 400:
                        for key in result.json().keys():
                            if key == "non_field_errors":
                                form.add_error(field=None, error=result.json()[key])
                            else:
                                form.add_error(field=key, error=result.json()[key])
                        return super().form_invalid(form)
                    if result.status_code == 404:
                        messages.error(self.request, 'Connection cannot be established to Wipani! [404]')
                        return self.render_to_response(self.get_context_data(form=form))
                    if validateJSON(result.content):
                        error = ''
                        for key in result.json().keys():
                            error = error + '<br/>' + result.json()[key]
                        messages.error(self.request, mark_safe('Wipani Error [' + str(result.status_code) + ']:' + error))
                    else:
                        messages.error(self.request, 'Wipani Error [' + str(result.status_code) + ']')
            else:
                form.add_error(field='email', error='Email is required.')
        return self.render_to_response(self.get_context_data(form=form))


class WipaniListView(IsWipaniAuthenticated, generic.ListView):
    template_name = 'repo/wipani/repolist.html'
    context_object_name = 'process_groups'

    def get_queryset(self):
        return Process_Group.objects.none()

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        try:
            result = requests.get(settings.WIPANI_URL + 'list/',
                                  headers={'Authorization': 'Token ' + self.token}
                                  )
        except (requests.exceptions.ConnectionError, requests.exceptions.ConnectTimeout):
            messages.error(self.request, 'Connection cannot be established to Wipani! Please try again later.')
            return ctx
        except Exception as e:
            messages.error(self.request, e)
            return ctx
        if result.status_code == 200:
            ctx['process_groups'] = result.json()
            return ctx
        else:
            if result.status_code == 401:
                messages.error(
                    self.request,
                    mark_safe("Wipani Authentication Token seems to be Incorrect! Please correct the Authentication Token in <a href='/repository/wipani/token/'>Wipani Settings</a>!")
                    )
                return ctx
            if result.status_code == 404:
                messages.error(self.request, 'Connection cannot be established to Wipani! [404]')
                return ctx
            if validateJSON(result.content):
                error = ''
                for key in result.json().keys():
                    error = error + '<br/>' + result.json()[key]
                messages.error(self.request, mark_safe('Wipani Error [' + str(result.status_code) + ']:' + error))
            else:
                messages.error(self.request, 'Wipani Error [' + str(result.status_code) + ']')
            return ctx


class WipaniDetailsView(IsWipaniAuthenticated, generic.TemplateView):
    template_name = 'repo/wipani/pg_details.html'
    context_object_name = 'process_group'

    def get(self, request, *args, **kwargs):
        context = {}
        try:
            result = requests.get(settings.WIPANI_URL + f'list/{kwargs.get("pk")}/',
                                  headers={'Authorization': 'Token ' + self.token}
                                  )
        except (requests.exceptions.ConnectionError, requests.exceptions.ConnectTimeout):
            messages.error(self.request, 'Connection cannot be established to Wipani! Please try again later.')
            return self.render_to_response(context)
        except Exception as e:
            messages.error(self.request, e)
            return self.render_to_response(context)

        if result.status_code == 200:
            context['process_group'] = result.json()
        else:
            if result.status_code == 401:
                messages.error(self.request, 'Wipani Authentication Token seems to be Incorrect! Please correct the Authentication Token!')
                return redirect(reverse('repo:WipaniToken')+'?next=' + self.request.path_info)
            if result.status_code == 404:
                messages.error(self.request, 'Could not be find Process Group!')
                return redirect('repo:WipaniList')
            if validateJSON(result.content):
                error = ''
                for key in result.json().keys():
                    error = error + '<br/>' + result.json()[key]
                messages.error(self.request, mark_safe('Wipani Error [' + str(result.status_code) + ']:' + error))
            else:
                messages.error(self.request, 'Wipani Error [' + str(result.status_code) + ']')
        return self.render_to_response(context)


class AjaxChildsDetailsView(IsWipaniAuthenticated, generic.View):
    @csrf_exempt
    def get(self, request, pk):
        data = {}
        c_type = self.request.GET.get('c_type', False)
        if self.request.GET.get('c_object', False):
            c_object = json.loads(self.request.GET.get('c_object'))
        else:
            c_object = False
        if not (c_type and c_object):
            raise Http404()

        data['html'] = render_to_string('repo/wipani/right_panel.html',
                                        {'c_type': c_type, 'instance': c_object})
        return JsonResponse(data)


class AjaxWipaniPushView(IsWipaniAuthenticated, generic.View):
    @csrf_exempt
    def get(self, request, pk):
        data = {'status': ''}
        pg = Process_Group.objects.get(id=pk)
        processes = []
        objectives = []
        risks = []
        controls = []
        tests = []

        for process in pg.process_group.all():
            temp = ProcessSerializer(process).data
            temp['child_id'] = process.id
            if process.wipani_pg_id:
                temp['wipani_pg_id'] = str(process.wipani_pg_id)
            processes.append(temp)

        for objective in pg.process_group_objective.all():
            temp = ObjectiveSerializer(objective).data
            temp['child_id'] = objective.id
            temp['processes'] = list(objective.process.all().values_list('id', flat=True))
            if objective.wipani_pg_id:
                temp['wipani_pg_id'] = str(objective.wipani_pg_id)
            objectives.append(temp)

        for risk in pg.process_group_risk.all():
            temp = RiskSerializer(risk).data
            temp['child_id'] = risk.id
            temp['objectives'] = list(risk.objective.all().values_list('id', flat=True))
            if risk.wipani_pg_id:
                temp['wipani_pg_id'] = str(risk.wipani_pg_id)
            risks.append(temp)

        for control in pg.process_group_control.all():
            temp = ControlSerializer(control).data
            temp['child_id'] = control.id
            temp['risks'] = list(control.risk.all().values_list('id', flat=True))
            if control.wipani_pg_id:
                temp['wipani_pg_id'] = str(control.wipani_pg_id)
            controls.append(temp)

        for test in pg.process_group_test.all():
            temp = TestSerializer(test).data
            temp['child_id'] = test.id
            temp['controls'] = list(test.control.all().values_list('id', flat=True))
            if test.wipani_pg_id:
                temp['wipani_pg_id'] = str(test.wipani_pg_id)
            tests.append(temp)

        data = {
            'title': pg.title,
            'desc': pg.desc,
            'changed_on': str(pg.changed_on),
            'processes': processes,
            'objectives': objectives,
            'risks': risks,
            'controls': controls,
            'tests': tests,
        }

        if pg.wipani_pg_id:
            data['wipani_pg_id'] = str(pg.wipani_pg_id)

        try:
            result = requests.post(settings.WIPANI_URL + 'push/',
                                   json.dumps(data),
                                   headers={'Authorization': 'Token ' + self.token}
                                   )
        except (requests.exceptions.ConnectionError, requests.exceptions.ConnectTimeout):
            messages.error(self.request, 'Connection cannot be established to Wipani! Please try again later.')
            data = {'status': 'ConnectionError'}
            return JsonResponse(data)
        except Exception as e:
            messages.error(self.request, e)
            data = {'status': 'ConnectionError'}
            return JsonResponse(data)

        if result.status_code == 201 or result.status_code == 202:
            pg.wipani_pg_id = result.json()['id']
            pg.save()

            for process in pg.process_group.all():
                try:
                    process.wipani_pg_id = result.json()['processes'][str(process.id)]
                    process.save()
                finally:
                    pass

            for objective in pg.process_group_objective.all():
                try:
                    objective.wipani_pg_id = result.json()['objectives'][str(objective.id)]
                    objective.save()
                finally:
                    pass

            for risk in pg.process_group_risk.all():
                try:
                    risk.wipani_pg_id = result.json()['risks'][str(risk.id)]
                    risk.save()
                finally:
                    pass

            for control in pg.process_group_control.all():
                try:
                    control.wipani_pg_id = result.json()['controls'][str(control.id)]
                    control.save()
                finally:
                    pass

            for test in pg.process_group_test.all():
                try:
                    test.wipani_pg_id = result.json()['tests'][str(test.id)]
                    test.save()
                finally:
                    pass

            messages.success(request, f"Successfully pushed {pg.title} to Wipani")
            data = {'status': 'success'}
            return JsonResponse(data)
        else:
            if result.status_code == 401:
                messages.error(self.request, 'Wipani Authentication Token seems to be Incorrect! Please correct the Authentication Token!')
                return redirect(reverse('repo:WipaniToken')+'?next=' + self.request.path_info)
            if result.status_code == 404:
                messages.error(self.request, 'Connection cannot be established to Wipani! [404]')
                data = {'status': 'ConnectionError'}
                return JsonResponse(data)
            if validateJSON(result.content):
                error = ''
                for key in result.json().keys():
                    error = error + '<br/>' + result.json()[key]
                messages.error(self.request, mark_safe('Wipani Error [' + str(result.status_code) + ']:' + error))
                data = {'status': result.json().get('error')}
            else:
                messages.error(self.request, 'Wipani Error [' + str(result.status_code) + ']')
                data = {'status': 'Error'}
            return JsonResponse(data)


class AjaxWipaniPullView(IsWipaniAuthenticated, generic.View):
    @csrf_exempt
    def get(self, request, pk):
        data = {'status': ''}
        try:
            result = requests.get(settings.WIPANI_URL + f'list/{pk}/pull/',
                                  headers={'Authorization': 'Token ' + self.token}
                                  )
        except (requests.exceptions.ConnectionError, requests.exceptions.ConnectTimeout):
            messages.error(self.request, 'Connection cannot be established to Wipani! Please try again later.')
            data = {'status': 'ConnectionError'}
            return JsonResponse(data)
        except Exception as e:
            messages.error(self.request, e)
            data = {'status': 'ConnectionError'}
            return JsonResponse(data)

        if result.status_code == 200:
            try:
                with transaction.atomic():
                    if result.status_code == 200:
                        data = result.json()
                        pg_serializer = ProcessGroupSerializer(data=data)
                        pg_serializer.is_valid(raise_exception=True)
                        pg = pg_serializer.save()
                        childs_temp = {}
                        for process_data in data['processes']:
                            process_serializer = ProcessSerializer(data=process_data)
                            process_serializer.is_valid(raise_exception=True)
                            process = process_serializer.save(pg=pg)
                            childs_temp[str(process_data['child_id'])] = process
                        childs = childs_temp
                        childs_temp = {}
                        for objective_data in data['objectives']:
                            objective_serializer = ObjectiveSerializer(data=objective_data)
                            objective_serializer.is_valid(raise_exception=True)
                            objective = objective_serializer.save(pg=pg)
                            for process_id in objective_data['processes']:
                                objective.process.add(childs[str(process_id)])
                            childs_temp[str(objective_data['child_id'])] = objective
                        childs = childs_temp
                        childs_temp = {}
                        for risk_data in data['risks']:
                            risk_serializer = RiskSerializer(data=risk_data)
                            risk_serializer.is_valid(raise_exception=True)
                            risk = risk_serializer.save(pg=pg)
                            for objective_id in risk_data['objectives']:
                                risk.objective.add(childs[str(objective_id)])
                            childs_temp[str(risk_data['child_id'])] = risk
                        childs = childs_temp
                        childs_temp = {}
                        for control_data in data['controls']:
                            control_serializer = ControlSerializer(data=control_data)
                            control_serializer.is_valid(raise_exception=True)
                            control = control_serializer.save(pg=pg)
                            for risk_id in control_data['risks']:
                                control.risk.add(childs[str(risk_id)])
                            childs_temp[str(control_data['child_id'])] = control
                        childs = childs_temp
                        childs_temp = {}
                        for test_data in data['tests']:
                            test_serializer = TestSerializer(data=test_data)
                            test_serializer.is_valid(raise_exception=True)
                            test = test_serializer.save(pg=pg)
                            for control_id in test_data['controls']:
                                test.control.add(childs[str(control_id)])
                            childs_temp[str(test_data['child_id'])] = test
                        messages.success(request, "Successfully pulled!")
                        data = {'status': 'success'}
                    else:
                        messages.success(request, "Failed to pull!")
                        data = {'status': 'fail'}
            except Exception as e:
                messages.success(request, "Failed to pull!")
                data = {'status': str(e)}
            return JsonResponse(data)
        else:
            messages.error(request, "Failed to pull!")
            if result.status_code == 401:
                messages.error(self.request, 'Wipani Authentication Token seems to be Incorrect! Please correct the Authentication Token!')
                return redirect(reverse('repo:WipaniToken')+'?next=' + self.request.path_info)
            if result.status_code == 404:
                messages.error(self.request, 'Connection cannot be established to Wipani! [404]')
                data = {'status': 'ConnectionError'}
                return JsonResponse(data)
            data = {'status': 'failed'}
            if validateJSON(result.content):
                error = ''
                for key in result.json().keys():
                    error = error + '<br/>' + result.json()[key]
                messages.error(self.request, mark_safe('Wipani Error [' + str(result.status_code) + ']:' + error))
            else:
                messages.error(self.request, 'Wipani Error [' + str(result.status_code) + ']')
            return JsonResponse(data)


class AjaxWipaniDeleteView(IsWipaniAuthenticated, generic.View):
    @csrf_exempt
    def get(self, request, pk, name):
        data = {'status': ''}
        try:
            result = requests.delete(settings.WIPANI_URL + f'list/{pk}/delete/',
                                     headers={'Authorization': 'Token ' + self.token}
                                     )
        except (requests.exceptions.ConnectionError, requests.exceptions.ConnectTimeout):
            messages.error(self.request, 'Connection cannot be established to Wipani! Please try again later.')
            data = {'status': 'ConnectionError'}
            return JsonResponse(data)
        except Exception as e:
            messages.error(self.request, e)
            data = {'status': 'ConnectionError'}
            return JsonResponse(data)

        if result.status_code == 204:
            for pg in Process_Group.objects.filter(wipani_pg_id=pk):
                pg.wipani_pg_id = None
                pg.save()

                for process in pg.process_group.all():
                    process.wipani_pg_id = None
                    process.save()

                for objective in pg.process_group_objective.all():
                    objective.wipani_pg_id = None
                    objective.save()

                for risk in pg.process_group_risk.all():
                    risk.wipani_pg_id = None
                    risk.save()

                for control in pg.process_group_control.all():
                    control.wipani_pg_id = None
                    control.save()

                for test in pg.process_group_test.all():
                    test.wipani_pg_id = None
                    test.save()

            data = {'status': 'success'}
            messages.success(request, f"{name} was successfully deleted from Wipani!")
        else:
            messages.error(request, f"Failed to delete {name} from Wipani!")
            if result.status_code == 401:
                messages.error(self.request, 'Wipani Authentication Token seems to be Incorrect! Please correct the Authentication Token!')
                return redirect(reverse('repo:WipaniToken')+'?next=' + self.request.path_info)
            if result.status_code == 404:
                messages.error(self.request, 'Connection cannot be established to Wipani! [404]')
                data = {'status': 'ConnectionError'}
                return JsonResponse(data)
            data = {'status': 'failed'}
            if validateJSON(result.content):
                error = ''
                for key in result.json().keys():
                    error = error + '<br/>' + result.json()[key]
                messages.error(self.request, mark_safe('Wipani Error [' + str(result.status_code) + ']:' + error))
            else:
                messages.error(self.request, 'Wipani Error [' + str(result.status_code) + ']')
            return JsonResponse(data)
