from django.contrib.contenttypes.fields import GenericForeignKey, \
    GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models.signals import pre_delete
from django.forms import ValidationError


def validate_token_length(value):
    if len(value) != 40:
        raise ValidationError("Invalid Token! Token should have a length of 40 characters!")


class WipaniToken(models.Model):
    token = models.CharField(max_length=40, validators=[validate_token_length])

    def __str__(self):
        return self.token


class Attachment(models.Model):
    workpaper = models.FileField('Workpapers', upload_to='attachments/')
    filename = models.CharField(max_length=200)
    content_type = models.ForeignKey(ContentType, related_name='repo_attachment', on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')


def get_childs(obj, pg_list):
    pgs = obj.repo_process_Group_belong_to.all()
    if pgs.exists():
        for pg in pgs:
            pg_list = get_childs(pg, pg_list)
    else:
        pg_list.append(obj.id)
    return pg_list


def get_all_childs(obj, pg_list):
    pgs = obj.repo_process_Group_belong_to.all()
    if pgs.exists():
        for pg in pgs:
            if not pg.process_group.all().exists():
                pg_list.append(pg.id)
                pg_list = get_all_childs(pg, pg_list)
    return pg_list


def get_process_groups(obj, pg_list):
    if obj.belong_to:
        pg_list.append(obj.belong_to_id)
        pg_list = get_process_groups(obj.belong_to, pg_list)
    pg_list = get_all_childs(obj, pg_list)
    return pg_list


class Process_Group(models.Model):
    title = models.CharField(max_length=200)
    desc = models.TextField('process group description', blank=True)
    changed_by = models.ForeignKey('auth.User', null=True, on_delete=models.SET_NULL)
    changed_on = models.DateTimeField(auto_now=True)
    belong_to = models.ForeignKey('self', related_name='repo_process_Group_belong_to', null=True, blank=True, on_delete=models.SET_NULL)
    wipani_pg_id = models.PositiveIntegerField(null=True, blank=True)

    class Meta:
        ordering = ['title']

    def __str__(self):
        return self.title

    def get_child_list(self):
        pg_list = []
        return get_childs(self, pg_list)

    def get_all_process_groups(self):
        pg_list = [self.id]
        return get_process_groups(self, pg_list)


class Process(models.Model):
    pg = models.ForeignKey(Process_Group, related_name="process_group", null=True, on_delete=models.SET_NULL)
    title = models.CharField(max_length=200)
    ref_id = models.CharField('reference ID', max_length=200, null=True)
    desc = models.TextField('process description', blank=True)
    owner = models.ForeignKey('auth.User', related_name='repo_process_owner',
                              null=True, blank=True, on_delete=models.SET_NULL)
    changed_by = models.ForeignKey('auth.User',
                                   related_name='repo_process_changed_by', null=True, on_delete=models.SET_NULL)
    changed_on = models.DateTimeField(auto_now=True)
    papers = GenericRelation(Attachment)
    wipani_pg_id = models.PositiveIntegerField(null=True, blank=True)

    class Meta:
        ordering = ['title']

    def __str__(self):
        if self.ref_id:
            return self.title + " - " + self.ref_id
        else:
            return self.title

    def get_childs(self):
        return self.obj_process.all()


class Objective(models.Model):
    pg = models.ForeignKey(Process_Group, related_name="process_group_objective", null=True, on_delete=models.SET_NULL)
    process = models.ManyToManyField(Process, verbose_name="related processes",
                                     blank=False, related_name="obj_process")
    title = models.CharField(max_length=200)
    ref_id = models.CharField('reference ID', max_length=200, null=True)
    desc = models.TextField('objective description', blank=True)
    changed_by = models.ForeignKey('auth.User',
                                   related_name='repo_objective_changed_by', null=True, on_delete=models.SET_NULL)
    changed_on = models.DateTimeField(auto_now=True)
    papers = GenericRelation(Attachment)
    wipani_pg_id = models.PositiveIntegerField(null=True, blank=True)

    class Meta:
        ordering = ['title']

    def __str__(self):
        if self.ref_id:
            return self.title + " - " + self.ref_id
        else:
            return self.title

    def get_childs(self):
        return self.risk_obj.all()

    def get_parents(self):
        return self.process.all()


class Risk(models.Model):
    AVOID = "AVD"
    REDUCE = "RED"
    SHARE = "SHR"
    RETAIN = "RTN"
    treatment_type = (
        (RETAIN, 'Retention (accept and budget)'),
        (AVOID, 'Avoidance (eliminate, withdraw from or not become involved)'),
        (REDUCE, 'Reduction (optimise, mitigate)'),
        (SHARE, 'Sharing (transfer, outsource or insure)'),
    )
    pg = models.ForeignKey(Process_Group, related_name="process_group_risk", null=True, on_delete=models.SET_NULL)
    objective = models.ManyToManyField(Objective, related_name="risk_obj",
                                       verbose_name="related objectives", blank=False)
    title = models.CharField(max_length=200)
    ref_id = models.CharField('reference ID', max_length=200, null=True)
    desc = models.TextField('risk description', blank=True)
    treatment = models.CharField(max_length=3,
                                 choices=treatment_type,
                                 default=AVOID)
    changed_by = models.ForeignKey('auth.User',
                                   related_name='repo_risk_changed_by', null=True, on_delete=models.SET_NULL)
    changed_on = models.DateTimeField(auto_now=True)
    papers = GenericRelation(Attachment)
    wipani_pg_id = models.PositiveIntegerField(null=True, blank=True)

    class Meta:
        ordering = ['title']

    def __str__(self):
        if self.ref_id:
            return self.title + " - " + self.ref_id
        else:
            return self.title

    def get_childs(self):
        return self.control_risk.all()

    def get_parents(self):
        return self.objective.all()


class Control(models.Model):
    # TODO: move these constants to core mod
    KEY = "KY"
    SECONDARY = "SC"
    con_type = (
        (KEY, 'Key Control'),
        (SECONDARY, 'Secondary Control'),
    )
    PREVENTIVE = "PR"
    DETECTIVE = "DT"
    con_class = (
        (PREVENTIVE, 'Preventive Control'),
        (DETECTIVE, 'Detective Control'),
    )
    ANNUALLY = "AN"
    QUARTERLY = "QU"
    MONTHLY = "MN"
    WEEKLY = "WK"
    DAILY = "DY"
    MANY = "MY"
    AS_NEEDED = "AN"
    freq_type = (
        (ANNUALLY, 'Annually'),
        (QUARTERLY, 'Quarterly'),
        (MONTHLY, 'Monthly'),
        (WEEKLY, 'Weekly'),
        (DAILY, 'Daily'),
        (MANY, 'Many Times per Day'),
        (AS_NEEDED, 'As Needed'),
    )
    pg = models.ForeignKey(Process_Group, related_name="process_group_control", null=True, on_delete=models.SET_NULL)
    risk = models.ManyToManyField(Risk, verbose_name="related risks",
                                  blank=False, related_name="control_risk")
    title = models.CharField(max_length=200)
    ref_id = models.CharField('reference ID', max_length=200, null=True)
    desc = models.TextField('control description', blank=True)
    control_type = models.CharField(max_length=2,
                                    choices=con_type,
                                    default=KEY)
    control_class = models.CharField(max_length=2,
                                     choices=con_class,
                                     default=PREVENTIVE)
    frequency = models.CharField(max_length=2,
                                 choices=freq_type,
                                 default=ANNUALLY)
    owner = models.ForeignKey('auth.User', null=True, blank=True, related_name='repo_control_owner', on_delete=models.SET_NULL)
    changed_by = models.ForeignKey('auth.User',
                                   related_name='repo_control_changed_by', null=True, on_delete=models.SET_NULL)
    changed_on = models.DateTimeField(auto_now=True)
    papers = GenericRelation(Attachment)
    wipani_pg_id = models.PositiveIntegerField(null=True, blank=True)

    class Meta:
        ordering = ['title']

    def __str__(self):
        if self.ref_id:
            return self.title + " - " + self.ref_id
        else:
            return self.title

    def get_childs(self):
        return self.test_control.all()

    def get_parents(self):
        return self.risk.all()


class Test(models.Model):
    #    PASSED = "PS"
    #    FAILED = "FL"
    #    status_choices = (
    #        (PASSED, 'Passed'),
    #        (FAILED, 'Failed'),
    #        )
    pg = models.ForeignKey(Process_Group, related_name="process_group_test", null=True, on_delete=models.SET_NULL)
    control = models.ManyToManyField(Control, verbose_name="related controls",
                                     blank=False, related_name="test_control")
    title = models.CharField(max_length=200)
    ref_id = models.CharField('reference ID', max_length=200, null=True)
    desc = models.TextField('test description', blank=True)
    #    result = models.CharField(max_length=2,
    #                              choices=status_choices,
    #                            default=FAILED)
    #    result_desc = models.TextField('test result description', blank=True)
    changed_by = models.ForeignKey('auth.User',
                                   related_name='repo_test_changed_by', null=True, on_delete=models.SET_NULL)
    changed_on = models.DateTimeField(auto_now=True)
    papers = GenericRelation(Attachment)
    wipani_pg_id = models.PositiveIntegerField(null=True, blank=True)

    class Meta:
        ordering = ['title']

    def __str__(self):
        if self.ref_id:
            return self.title + " - " + self.ref_id
        else:
            return self.title

    def get_parents(self):
        return self.control.all()


def delete_attachment(sender, instance, **kwargs):
    instance.workpaper.delete()


pre_delete.connect(delete_attachment, sender=Attachment)
