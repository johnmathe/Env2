from django.contrib import admin

from .models import Process_Group, Process, Objective, Risk, Control, Test, Attachment, WipaniToken

admin.site.register(Attachment)
admin.site.register(Process_Group)
admin.site.register(Process)
admin.site.register(Objective)
admin.site.register(Risk)
admin.site.register(Control)
admin.site.register(Test)
admin.site.register(WipaniToken)
