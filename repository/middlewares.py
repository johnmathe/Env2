from django.utils.deprecation import MiddlewareMixin

from repository.models import WipaniToken


class WipaniAuthenticationMiddleware(MiddlewareMixin):
    def process_request(self, request):
        if WipaniToken.objects.all().exists():
            request.is_wipani_authenticated = True
        else:
            request.is_wipani_authenticated = False
