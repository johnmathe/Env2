# from django.conf.urls import url
from django.urls import path
from django.contrib.auth.decorators import login_required
from enum import Enum

from repository.views.base import AjaxAttachmentDeleteView, AjaxChildsAddEditView, AjaxChildsDeleteView, ProcessGroupCreateView, ProcessGroupDetailView, ProcessGroupGetChilds, \
    ProcessGroupManagement, ReposIndexView
from repository.views.wipani import AjaxChildsDetailsView, AjaxWipaniDeleteView, AjaxWipaniPullView, AjaxWipaniPushView, TokenView, RegisterView, WipaniDetailsView, WipaniListView

app_name = "repo"

class ObjType1(Enum):
    PROCESS_GROUP = 'process_group'
    PROCESS = 'process'
    OBJECTIVE = 'objective'
    RISK = 'risk'
    CONTROL = 'control'
    TEST = 'test'

urlpatterns = [
    path('', login_required(ReposIndexView.as_view()), name='repo_index'),
    path('process-group/create/', login_required(ProcessGroupCreateView.as_view()), name='pg_create'),
    path('process-group/<int:process_group_id>/', login_required(ProcessGroupDetailView.as_view()), name='process_group'),
    path('process-group/<int:process_group_id>/management/', login_required(ProcessGroupManagement.as_view()),
        name='pg_management'),
    path('process-group/<int:process_group_id>/management/<str:ctype>process_group|process|objective|risk|control|test)/<int:object_id>/childs/',
        login_required(ProcessGroupGetChilds.as_view()), name='pg_childs'),
    path('process-group/<int:process_group_id>/management/<str:ctype>process_group|process|objective|risk|control|test)/add/',
        login_required(AjaxChildsAddEditView.as_view()), name='pg_childs_add'),
    path('process-group/<int:process_group_id>/management/<str:ctype>process_group|process|objective|risk|control|test)/<int:object_id>/edit/',
        login_required(AjaxChildsAddEditView.as_view()), name='pg_childs_edit'),
    path('process-group/<int:process_group_id>/management/<str:ctype>process_group|process|objective|risk|control|test)/<int:object_id>/delete/',
        login_required(AjaxChildsDeleteView.as_view()), name='pg_childs_delete'),

    path('attachment/<int:attachment_id>/delete/', login_required(AjaxAttachmentDeleteView.as_view()),
        name='ajax_attachment_delete'),

    path('wipani/register/', RegisterView.as_view(), name='WipaniRegister'),
    path('wipani/token/', TokenView.as_view(), name='WipaniToken'),
    path('wipani/list/', WipaniListView.as_view(), name='WipaniList'),
    path('wipani/list/<int:pk>/', WipaniDetailsView.as_view(), name='WipaniDetails'),
    path('wipani/list/<int:pk>/details/', AjaxChildsDetailsView.as_view(), name='AjaxChildsDetails'),
    path('wipani/list/<int:pk>/pull/', AjaxWipaniPullView.as_view(), name='AjaxWipaniPullView'),
    path('wipani/list/<int:pk>/delete/<str:name>/', AjaxWipaniDeleteView.as_view(), name='AjaxWipaniDeleteView'),
    path('process-group/<int:pk>/wipani/push/', AjaxWipaniPushView.as_view(), name='AjaxWipaniPush'),
]
