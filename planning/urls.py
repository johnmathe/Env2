# -*- coding: utf-8 -*-
# from django.conf.urls import url
from django.urls import path, include

from planning.views import PlanningView, PlanningAuditMonthView, PlanningDayView

urlpatterns = [
    path('', PlanningView.as_view(), name="planning_list"),
    path('day/', PlanningDayView.as_view(), name="planning_day"),
    path('audits/', PlanningAuditMonthView.as_view(), name="planning_audits"),
]
