# from django.conf.urls import url
from django.urls import path, include
from django.contrib.auth.decorators import login_required

from repository.views.base import RepoCopyView
from . import views
from .views import RepoCopy, AuditCopyPushRepo, AuditCopyView

urlpatterns = [
    path('', login_required(views.IndexView.as_view()), name='index'),
    path('create/', login_required(views.audit_create), name='audit_create'),
    path('audit/<int:audit_id>/copy-audit/', login_required(
        AuditCopyView.as_view()), name='audit_copy'),
    path('audit/<int:audit_id>/copy-repo/', login_required(
        RepoCopyView.as_view()), name='repo_copy'),
    path('audit/<int:audit_id>/copy-repos/', login_required(
        RepoCopy.as_view()), name='repo_copy_view'),
    path('audit/<int:audit_id>/push-repo/', login_required(
        AuditCopyPushRepo.as_view()), name='audit_push_repo'),

    path('audit/<int:audit_id>/', login_required(views.audit), name='audit'),
    path('not_implemented', login_required(views.not_implemented), name='not_implemented'),

    # path('audit/process/create/', login_required(views.process),
    #     name='process_create'),
    # path('audit/process/<int:process_id>/',
    #     login_required(views.process), name='audit_process'),
    # path('audit/objective/create/', login_required(views.objective),
    #     name='objective_create'),
    # path('audit/objective/<int:objective_id>/',
    #     login_required(views.objective), name='audit_objective'),
    # path('audit/risk/create/', login_required(views.risk), name='risk_create'),
    # path('audit/risk/<int:risk_id>/', login_required(views.risk),
    #     name='audit_risk'),
    # path('audit/<int:audit_id>/control/create/',
    #     login_required(views.control_create), name='control_create'),
    # path('audit/control/<int:control_id>/',
    #     login_required(views.control), name='audit_control'),

    path('audit/<int:audit_id>/execute/<str:view_type>', #process|objective|risk|control|test
        login_required(views.AuditExecuteView.as_view()), name='audit_execute'),
    path('audit/process/<int:process_id>/objective/', login_required(views.process_objective),
        name='audit_process_objective'),
    path('audit/objective/<int:objective_id>/risks/',
        login_required(views.objective_risk), name='audit_objective_risk'),
    path('audit/risk/<int:risk_id>/controller/',
        login_required(views.risk_controller), name='audit_risk_controller'),
    path('audit/control/<int:control_id>/test/',
        login_required(views.controller_test), name='audit_controller_test'),
    path('audit/test/<int:test_id>/findings/',
        login_required(views.test_findings), name='audit_test_findings'),
    path('audit/test/<int:test_id>/ajax/',
        login_required(views.AjaxTestResultView.as_view()),
        name='audit_ajax_test_form'),
    path('audit/finding/ajax-add/',
        login_required(views.AjaxFindingView.as_view()),
        name='ajax_finding_view'),
    path('attachment/<int:attachment_id>/delete/',
        login_required(views.AjaxAttachmentDeleteView.as_view()),
        name='ajax_attachment_delete'),
    path('audit/<int:audit_id>/management/', login_required(views.AuditChildView.as_view()),
        name='audit_management'),
    # Process Management
    path('audit/<int:audit_id>/process/add/', login_required(views.AjaxProcessAddEditView.as_view()),
        name='ajax_process_add'),
    path('audit/<int:audit_id>/process/<int:process_id>/edit/', login_required(views.AjaxProcessAddEditView.as_view()),
        name='ajax_process_edit'),
    path('process/<int:process_id>/delete/', login_required(views.AjaxProcessDeleteView.as_view()),
        name='ajax_process_delete'),
    # Objective Management
    path('audit/<int:audit_id>/objective/add/', login_required(views.AjaxObjectiveAddEditView.as_view()),
        name='ajax_objective_add'),
    path('audit/<int:audit_id>/objective/<int:objective_id>/edit/', login_required(views.AjaxObjectiveAddEditView.as_view()),
        name='ajax_process_edit'),
    path('objective/<int:objective_id>/delete/', views.AjaxObjectiveDeleteView.as_view(),
        name='ajax_objective_delete'),
    # Risk Management
    path('audit/<int:audit_id>/risk/add/', login_required(views.AjaxRiskAddEditView.as_view()),
        name='ajax_risk_add'),
    path('audit/<int:audit_id>/risk/<int:risk_id>/edit/', login_required(views.AjaxRiskAddEditView.as_view()),
        name='ajax_risk_edit'),
    path('risk/<int:risk_id>/delete/', login_required(views.AjaxRiskDeleteView.as_view()),
        name='ajax_risk_delete'),
    # Control Management
    path('audit/<int:audit_id>/control/add/', login_required(views.AjaxControlAddEditView.as_view()),
        name='ajax_control_add'),
    path('audit/<int:audit_id>/control/<int:control_id>/edit/', login_required(views.AjaxControlAddEditView.as_view()),
        name='ajax_control_edit'),
    path('control/<int:control_id>/delete/', login_required(views.AjaxControlDeleteView.as_view()),
        name='ajax_control_delete'),
    # Test Management
    path('audit/<int:audit_id>/test/add/', login_required(views.AjaxTestAddEditView.as_view()),
        name='ajax_test_add'),
    path('audit/<int:audit_id>/test/<int:test_id>/edit/', login_required(views.AjaxTestAddEditView.as_view()),
        name='ajax_test_edit'),
    path('test/<int:test_id>/delete/', login_required(views.AjaxTestDeleteView.as_view()),
        name='ajax_test_delete'),
    # Report Generation
    path('audit/<int:pk>/report/', login_required(views.AuditReport.as_view()),
        name='audit_report'),
]
