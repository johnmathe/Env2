from django.contrib.auth.management.commands import createsuperuser
from django.contrib.auth import get_user_model

User = get_user_model()


class Command(createsuperuser.Command):

    def handle(self, *args, **options):
        if User.objects.count() == 0:
            username = 'admin'
            email = 'admin@gmail.com'
            password = 'admin'
            print('Creating superuser account for %s' % (username))
            User.objects.create_superuser(username=username, email=email, password=password)
            print('Created superuser account for %s' % (username))
        else:
            print('Admin accounts can only be initialized if no Accounts exist')
