from datetime import date
from django.contrib import messages
from django.core.files import File
from django.forms import inlineformset_factory
from django.http import JsonResponse
from django.http.response import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext
from django.views import generic
from django.views.decorators.csrf import csrf_exempt

from .forms import AuditForm, AjaxTestResultForm, AjaxFindingForm, \
    AjaxProcessForm, AjaxObjectiveForm, AjaxRiskForm, AjaxControlForm, \
    AjaxTestForm
from .models import Process, Objective, Risk, Control, Test, \
    Finding, Audit, Attachment

from repository.models import Process_Group, Process as RP, \
    Objective as RO, Risk as RR, Control as RC, Test as RT, Attachment as RAttachment


class IndexView(generic.ListView):
    template_name = 'audits/index.html'
    context_object_name = 'all_audits'

    def get_queryset(self):
        return Audit.objects.order_by('-start_date')[:10]

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        year = date.today().year - 2
        y = []
        x = []
        for i in range(3):
            a = Audit.objects.filter(fy=int(year) + i, status='AC').count()
            d = Audit.objects.filter(fy=int(year) + i, status='DR').count()
            c = Audit.objects.filter(fy=int(year) + i, status='CL').count()
            y.append([a, d, c])
            x.append(int(year) + i)
        context['x'] = x
        context['y'] = y
        return context


class LoginView(generic.TemplateView):
    template_name = 'login.html'

    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        if 'next' in list(self.request.GET.keys()):
            context['next'] = self.request.GET['next']
        return context


def index(request):
    all_audits = Audit.objects.order_by('-start_date')[:10]
    context = {'all_audits': all_audits}
    year = date.today().year - 2
    y = []
    x = []
    for i in range(3):
        a = Audit.objects.filter(fy=int(year) + i, status='AC').count()
        b = Audit.objects.filter(fy=int(year) + i, status='DR').count()
        c = Audit.objects.filter(fy=int(year) + i, status='CL').count()
        y.append([a, b, c])
        x.append(int(year) + i)
    context['x'] = x
    context['y'] = y
    return render(request, 'audits/index.html', context)


def not_implemented(request):
    return render(request, 'not_implemented.html')


class AuditChildView(generic.ListView):
    template_name = 'audits/audit_childs.html'

    # paginate_by = 5

    def get_queryset(self):
        queryset = Process.objects.filter(audit_id=self.kwargs['audit_id'])
        return queryset

    def get_context_data(self, **kwargs):
        context = super(AuditChildView, self).get_context_data(**kwargs)
        context['type'] = 'process'
        context['audit_id'] = self.kwargs['audit_id']
        context['audit'] = Audit.objects.get(id=int(self.kwargs['audit_id']))
        context['management'] = True
        return context


def audit(request, audit_id):
    aud = get_object_or_404(Audit, pk=audit_id)
    if request.method == "POST":
        form = AuditForm(request.POST, request.FILES, instance=aud)

        if form.is_valid():
            audit = form.save(commit=False)
            audit.changed_by = request.user
            audit.changed_on = timezone.now()
            audit = form.save()
            messages.success(request, 'Audit information was saved')
            papers = request.FILES.getlist('papers')
            try:
                if papers:
                    Attachment.objects.bulk_create(
                        [Attachment(content_object=audit,
                                    workpaper=attachment, filename=attachment.name)
                         for attachment in papers])

                messages.success(request, 'Attachment saved')
            except:
                messages.error(request, 'Attachment NOT saved')
                pass

            if request.POST.get('add_from_repo') == "":
                messages.info(request, 'add from repo')
                return render(request, 'audits/not_implemented.html')

            elif request.POST.get('copy_from_audit') == "":
                messages.info(request, 'copy from audit')
                return HttpResponseRedirect(
                    reverse('audits:audit_copy', args=[audit.id]))

            elif request.POST.get('add_manually') == "":
                messages.info(request, 'You may now add processes')
                # messages.debug(request, "add manually debug")
                return HttpResponseRedirect(reverse('audits:audit_management',
                                                    args=[audit.id]))

            elif request.POST.get('delete') == "":
                aud.delete()
                messages.success(request, 'Audit deleted')
                # messages.debug(request, "add manually debug")
                return HttpResponseRedirect(reverse('audits:index'))

            else:
                return HttpResponseRedirect(reverse('audits:audit', args=(audit.id,)))
                # return render(request, "audits/audit.html", {'form': form})

        else:
            messages.error(request, 'Your audit was NOT saved')
            return render(request, "audits/audit.html", {'form': form})
    else:
        form = AuditForm(instance=aud)
        return render(request, "audits/audit.html", {'form': form})


def audit_create(request):
    if request.method == "POST":
        form = AuditForm(request.POST)
        if form.is_valid():
            audit = form.save(commit=False)
            audit.changed_by = request.user
            audit.changed_on = timezone.now()
            audit = form.save()
            messages.success(request, 'Audit was saved')
            papers = request.FILES.getlist('papers')
            try:
                if papers:
                    Attachment.objects.bulk_create(
                        [Attachment(content_object=audit,
                                    workpaper=attachment, filename=attachment.name)
                         for attachment in papers])
            except:
                pass

            if request.POST.get('add_from_repo') == "":
                messages.success(request, "add from Repo")
                # return render(request, "audits/add_from_repo.html",
                # {'form': form})
                return HttpResponseRedirect("audits/not_implemented")

            elif request.POST.get('add_manually') == "":
                messages.success(request, "add manually")
                return HttpResponseRedirect(reverse('audits:audit_management',
                                                    args=[audit.id]))

            else:
                messages.success(request, "Audit was created")
                return HttpResponseRedirect(reverse('audits:audit',
                                                    args=(audit.id,)))

        else:
            messages.error(request, 'Your audit was NOT saved')
            return render(request, "audits/audit.html", {'form': form})

    else:
        form = AuditForm()
        return render(request, "audits/audit.html", {'form': form})


def person_view(request, audit_id=None):
    if audit_id == None:
        audit = Audit()
    else:
        audit = Audit.objects.get(id=audit_id)

    ProcessFormSet = inlineformset_factory(Audit, Process, fields=('title',), can_delete=True)

    if request.method == "POST":
        auditform = AuditForm(request.POST, instance=audit)
        processformset = ProcessFormSet(request.POST, request.FILES, instance=audit)

        if auditform.is_valid() and processformset.is_valid():
            auditform.save()
            processformset.save()

            # Redirect to somewhere
            if '_save' in request.POST:
                return HttpResponseRedirect('/audits/audit/process/create')
            if '_addanother' in request.POST:
                return HttpResponseRedirect('/audits/audit/process/create')

    else:
        auditform = AuditForm(instance=audit)
        processformset = ProcessFormSet(instance=audit)

    return render('process2.html', {
        'auditform': auditform,
        'processformset': processformset,
    })


# @permission_required('audits.add_process', 'audits.change_process')
# def process(request, process_id=None):
#     if process_id != None:
#         pro = get_object_or_404(Process, pk=process_id)
#         if request.method == "POST":
#             form = ProcessForm(request.POST, instance=pro)
#             if form.is_valid():
#                 process = form.save(commit=False)
#                 process.changed_by = request.user
#                 process.changed_on = timezone.now()
#                 process = form.save()
#                 messages.success(request, 'Your process was saved')
#                 return HttpResponseRedirect(reverse('audits:audit_process', args=(process.id,)))
#             else:
#                 messages.error(request, 'Your process was NOT saved')
#                 return render(request, "audits/process.html", {'form': form})
#         else:
#             form = ProcessForm(instance=pro)
#             return render(request, "audits/process.html", {'form': form})
#     else:
#         if request.method == "POST":
#             form = ProcessForm(request.POST)
#             if form.is_valid():
#                 process = form.save(commit=False)
#                 process.changed_by = request.user
#                 process.changed_on = timezone.now()
#                 process = form.save()
#                 messages.success(request, 'Your process was saved')
#                 return HttpResponseRedirect(reverse('audits:audit_process', args=(process.id,)))
#             else:
#                 return render(request, "audits/process.html", {'form': form})
#
#         else:
#             form = ProcessForm()
#             return render(request, "audits/process.html", {'form': form})
#
#
# @permission_required('audits.add_objecive', 'audits.change_objective')
# def objective(request, objective_id=None):
#     if objective_id != None:
#         obj = get_object_or_404(Objective, pk=objective_id)
#         if request.method == "POST":
#             form = ObjectiveForm(request.POST, instance=obj)
#             if form.is_valid():
#                 objective = form.save(commit=False)
#                 objective.changed_by = request.user
#                 objective.changed_on = timezone.now()
#                 objective = form.save()
#                 messages.success(request, 'Your objective was saved')
#                 return HttpResponseRedirect(reverse('audits:audit_objective', args=(objective.id,)))
#             else:
#                 return render(request, "audits/objective.html", {'form': form})
#         else:
#             form = ObjectiveForm(instance=obj)
#             return render(request, "audits/objective.html", {'form': form})
#
#     else:
#         if request.method == "POST":
#             form = ObjectiveForm(request.POST)
#             if form.is_valid():
#                 objective = form.save(commit=False)
#                 objective.changed_by = request.user
#                 objective.changed_on = timezone.now()
#                 objective = form.save()
#                 messages.success(request, 'Your objective was saved')
#                 return HttpResponseRedirect(reverse('audits:audit_objective', args=(objective.id,)))
#             else:
#                 messages.error(request, 'Your objective was not saved')
#                 return render(request, "audits/objective.html", {'form': form})
#         else:
#             form = ObjectiveForm()
#             return render(request, "audits/objective.html", {'form': form})
#
#
# @permission_required('audits.change_risk')
# def risk(request, risk_id):
#     ri = get_object_or_404(Risk, pk=risk_id)
#     if request.method == "POST":
#         form = RiskForm(request.POST, instance=ri)
#         if form.is_valid():
#             risk = form.save()
#             messages.success(request, 'Your risk was saved')
#             return HttpResponseRedirect(reverse('audits:risk', args=(risk.id,)))
#         else:
#             return render(request, "repository/risk.html", {'form': form})
#     else:
#         form = RiskForm(instance=ri)
#         return render(request, "repository/risk.html", {'form': form})
#
#
# @permission_required('repository.add_risk')
# def risk_create(request):
#     if request.method == "POST":
#         form = RiskForm(request.POST)
#         if form.is_valid():
#             risk = form.save()
#             messages.success(request, 'Your risk was saved')
#             return HttpResponseRedirect(reverse('audits:risk', args=(risk.id,)))
#         else:
#             return render(request, "repository/risk.html", {'form': form})
#
#     else:
#         form = RiskForm()
#         return render(request, "repository/risk.html", {'form': form})
#
#
# @permission_required('repository.change_control')
# def control(request, control_id):
#     con = get_object_or_404(Objective, pk=control_id)
#     if request.method == "POST":
#         form = ControlForm(request.POST, instance=con)
#         if form.is_valid():
#             control = form.save()
#             messages.success(request, 'Your control was saved')
#             return HttpResponseRedirect(reverse('audits:control', args=(control.id,)))
#         else:
#             return render(request, "repository/control.html", {'form': form})
#     else:
#         form = ControlForm(instance=con)
#         return render(request, "repository/control.html", {'form': form})
#
#
# @permission_required('repository.add_control')
# def control_create(request, audit_id):
#     if request.method == "POST":
#         form = ControlForm(request.POST)
#         if form.is_valid():
#             control = form.save()
#             messages.success(request, 'Your control was saved')
#             return HttpResponseRedirect(reverse('audits:control', args=(control.id,)))
#         else:
#             return render(request, "repository/control.html", {'form': form})
#
#     else:
#         form = ControlForm()
#         controls = Control.objects.filter(risk__objective__process__audit=audit_id)
#         return render(request, "audits/control.html", {'form': form,
#                                                        'controls': controls})
#
#
# @permission_required('repository.change_test')
# def test(request, test_id):
#     te = get_object_or_404(Test, pk=test_id)
#     if request.method == "POST":
#         form = TestForm(request.POST, instance=te)
#         if form.is_valid():
#             test = form.save()
#             messages.success(request, 'Your test was saved')
#             return HttpResponseRedirect(reverse('audits:test', args=(test.id,)))
#         else:
#             return render(request, "repository/test.html", {'form': form})
#     else:
#         form = TestForm(instance=te)
#         return render(request, "repository/test.html", {'form': form})
#
#
# @permission_required('repository.add_test')
# def test_create(request):
#     if request.method == "POST":
#         form = TestForm(request.POST)
#         if form.is_valid():
#             test = form.save()
#             messages.success(request, 'Your test was saved')
#             return HttpResponseRedirect(reverse('audits:test', args=(test.id,)))
#         else:
#             return render(request, "repository/test.html", {'form': form})
#
#     else:
#         form = TestForm()
#         return render(request, "repository/test.html", {'form': form})


def audit_execute(request, audit_id):
    # audit execute code to be replaced
    aud = get_object_or_404(Audit, pk=audit_id)
    if request.method == "POST":
        form = AuditForm(request.POST, instance=aud)

        if form.is_valid():
            audit = form.save(commit=False)
            audit.changed_by = request.user
            audit.changed_on = timezone.now()
            audit = form.save()
            messages.success(request, 'Audit information was saved')

            if request.POST.get('add_from_repo') == "":
                messages.info(request, 'add from repo')
                return HttpResponseRedirect(reverse('audits:not_implemented', ))

            elif request.POST.get('add_manually') == "":
                messages.info(request, 'You may now add processes')
                # messages.debug(request, "add manually debug")
                return HttpResponseRedirect(reverse('audits:process_create'))

            else:
                return HttpResponseRedirect(reverse('audits:audit', args=(audit.id,)))

        else:
            messages.error(request, 'Your audit was NOT saved')
            return render(request, "audits/audit.html", {'form': form})
    else:
        form = AuditForm(instance=aud)
        return render(request, "audits/audit.html", {'form': form})


class AuditExecuteView(generic.ListView):
    template_name = 'audits/execute.html'

    # paginate_by = 5

    def get_queryset(self):
        if self.kwargs['view_type'] == 'process':
            return Process.objects.filter(audit_id=self.kwargs['audit_id']) \
                .order_by('-changed_on').distinct()
        elif self.kwargs['view_type'] == 'objective':
            return Objective.objects.filter(audit_id=self.kwargs['audit_id']) \
                .order_by('-changed_on').distinct()
        elif self.kwargs['view_type'] == 'risk':
            return Risk.objects.filter(audit_id=self.kwargs['audit_id']) \
                .order_by('-changed_on').distinct()
        elif self.kwargs['view_type'] == 'control':
            return Control.objects.filter(audit_id=self.kwargs['audit_id']) \
                .order_by('-changed_on').distinct()
        elif self.kwargs['view_type'] == 'test':
            return Test.objects.filter(audit_id=self.kwargs['audit_id']) \
                .order_by('-changed_on').distinct()
        return []

    def get_context_data(self, **kwargs):
        context = super(AuditExecuteView, self).get_context_data(**kwargs)
        context['type'] = self.kwargs['view_type']
        context['audit_id'] = self.kwargs['audit_id']
        context['process_id'] = 0
        context['objective_id'] = 0
        context['risk_id'] = 0
        context['control_id'] = 0
        context['test_id'] = 0
        context['audit'] = Audit.objects.get(id=int(self.kwargs['audit_id']))
        return context


@csrf_exempt
def process_objective(request, process_id):
    data = {}
    management = request.GET.get('management', False)
    objectives = Process.objects.get(id=process_id).obj_process.all() \
        .order_by('-changed_on')
    data['html'] = render_to_string('audits/process_objective.html',
                                    {'objectives': objectives, 'process_id': process_id,
                                     'management': management})
    return JsonResponse(data)


@csrf_exempt
def objective_risk(request, objective_id):
    data = {}
    management = request.GET.get('management', False)
    process_id = request.GET.get('process')
    risks = Objective.objects.get(id=objective_id).risk_obj.all() \
        .order_by('-changed_on')
    data['html'] = render_to_string('audits/objective_risks.html',
                                    {'risks': risks, 'objective_id': objective_id,
                                     'process_id': process_id, 'management': management})
    return JsonResponse(data)


@csrf_exempt
def risk_controller(request, risk_id):
    data = {}
    management = request.GET.get('management', False)
    process_id = request.GET.get('process')
    objective_id = request.GET.get('objective')
    controllers = Risk.objects.get(id=risk_id).control_risk.all() \
        .order_by('-changed_on')
    data['html'] = render_to_string('audits/risk_controllers.html',
                                    {'controllers': controllers, 'risk_id': risk_id,
                                     'process_id': process_id, 'objective_id': objective_id,
                                     'management': management})
    return JsonResponse(data)


@csrf_exempt
def controller_test(request, control_id):
    data = {}
    process_id = request.GET.get('process')
    objective_id = request.GET.get('objective')
    risk_id = request.GET.get('risk')
    tests = Control.objects.get(id=control_id).test_control.all() \
        .order_by('-changed_on')
    if 'management' in list(request.GET.keys()):
        data['html'] = render_to_string('audits/controller_tests_list.html',
                                        {'tests': tests})
    else:
        data['html'] = render_to_string('audits/controller_tests.html',
                                        {'tests': tests, 'control_id': control_id, 'risk_id': risk_id,
                                         'process_id': process_id, 'objective_id': objective_id})
    return JsonResponse(data)


@csrf_exempt
def test_findings(request, test_id):
    data = {}
    process_id = request.GET.get('process')
    objective_id = request.GET.get('objective')
    risk_id = request.GET.get('risk')
    control_id = request.GET.get('control')
    find = Finding.objects.filter(test_id=test_id).values('id', 'title', 'desc')
    data['html'] = render_to_string('audits/test_findings.html',
                                    {'findings': find, 'test_id': test_id, 'control_id': control_id,
                                     'risk_id': risk_id, 'process_id': process_id,
                                     'objective_id': objective_id})
    return JsonResponse(data)


class AjaxTestResultView(generic.View):
    @csrf_exempt
    def get(self, request, test_id):
        data = {}
        test = Test.objects.get(id=test_id)
        form = AjaxTestResultForm(instance=test)
        csrf_token_value = request.COOKIES['csrftoken']
        data['html'] = render_to_string('audits/right_panel_test_result.html',
                                        {'form': form, 'test_id': test_id,
                                         'csrf_token_value': csrf_token_value})
        return JsonResponse(data)

    @csrf_exempt
    def post(self, request, test_id):
        data = {'status': 'failed'}
        test = Test.objects.get(id=test_id)
        form = AjaxTestResultForm(request.POST, instance=test)
        if form.is_valid():
            test = form.save()
            test.changed_by = request.user
            data['status'] = 'success'
        else:
            data['error'] = form.errors
        return JsonResponse(data)


class AjaxFindingView(generic.View):
    @csrf_exempt
    def get(self, request):
        data = {}
        test_id = request.GET.get('test_id')
        form = AjaxFindingForm()
        data = {'form': form, 'test_id': test_id}
        return render(request, 'audits/finding_form_ajax.html', data)

    @csrf_exempt
    def post(self, request):
        data = {'status': 'failed'}
        data['test'] = test_id = request.GET.get('test_id')
        test = Test.objects.get(id=test_id)
        form = AjaxFindingForm(request.POST)
        papers = request.FILES.getlist('papers')
        if form.is_valid():
            obj = form.save(commit=False)
            obj.changed_by = request.user
            obj.test = test
            obj.save()
            try:
                if papers:
                    Attachment.objects.bulk_create(
                        [Attachment(content_object=obj,
                                    workpaper=attachment, filename=attachment.name)
                         for attachment in papers])
            except:
                pass
            data['html'] = render_to_string('audits/ajax_bind_findings.html',
                                            {'obj': obj, 'process_id': 0, 'objective_id': 0, 'risk_id': 0,
                                             'control_id': 0, 'test_id': test_id})
            data['status'] = 'success'
        else:
            data['error'] = form.errors
        return JsonResponse(data)


class AjaxAttachmentDeleteView(generic.View):
    @csrf_exempt
    def get(self, request, attachment_id):
        data = {}
        Attachment.objects.get(id=attachment_id).delete()
        data['status'] = 'success'
        return JsonResponse(data)


class AjaxProcessAddEditView(generic.View):
    @csrf_exempt
    def get(self, request, audit_id, process_id=None):
        data = {}
        if process_id:
            obj = Process.objects.get(id=process_id)
            form = AjaxProcessForm(instance=obj)
        else:
            form = AjaxProcessForm()
        csrf_token_value = request.COOKIES['csrftoken']
        data['html'] = render_to_string('audits/right_panel_process.html',
                                        {'form': form, 'audit_id': audit_id, 'process_id': process_id,
                                         'csrf_token_value': csrf_token_value})
        return JsonResponse(data)

    @csrf_exempt
    def post(self, request, audit_id, process_id=None):
        data = {'status': 'failed'}
        audit = Audit.objects.get(id=audit_id)
        papers = request.FILES.getlist('papers')
        if process_id:
            obj = Process.objects.get(id=process_id)
            form = AjaxProcessForm(request.POST, instance=obj)
        else:
            form = AjaxProcessForm(request.POST)
        if form.is_valid():
            process = form.save(commit=False)
            if not process_id:
                process.audit = audit
                process.changed_by = request.user
            process.save()
            try:
                if papers:
                    Attachment.objects.bulk_create(
                        [Attachment(content_object=process,
                                    workpaper=attachment, filename=attachment.name)
                         for attachment in papers])
            except:
                pass
            data['status'] = 'success'
            data['process_title'] = process.title
            data['process_ref_id'] = process.ref_id
            data['process_desc'] = process.desc
            data['html'] = render_to_string('audits/ajax_binding_process.html',
                                            {'audit_id': audit_id, 'process': process})
        else:
            data['error'] = form.errors
        return JsonResponse(data)


class AjaxProcessDeleteView(generic.View):
    @csrf_exempt
    def get(self, request, process_id):
        data = {}
        Process.objects.get(id=process_id).delete()
        data['status'] = 'success'
        return JsonResponse(data)


class AjaxObjectiveAddEditView(generic.View):
    @csrf_exempt
    def get(self, request, audit_id, objective_id=None):
        audit = get_object_or_404(Audit, id=audit_id)
        data = {}
        if objective_id:
            obj = get_object_or_404(Objective, id=objective_id)
            form = AjaxObjectiveForm(instance=obj, audit=audit)
        else:
            form = AjaxObjectiveForm(audit=audit)
        csrf_token_value = request.COOKIES['csrftoken']
        data['html'] = render_to_string('audits/right_panel_objective.html',
                                        {'form': form, 'audit_id': audit_id, 'objective_id': objective_id,
                                         'csrf_token_value': csrf_token_value})
        return JsonResponse(data)

    @csrf_exempt
    def post(self, request, audit_id, objective_id=None):
        print("ok")
        data = {'status': 'failed'}
        audit = get_object_or_404(Audit, id=audit_id)
        papers = request.FILES.getlist('papers')
        if objective_id:
            obj = get_object_or_404(Objective, id=objective_id)
            form = AjaxObjectiveForm(request.POST, instance=obj, audit=audit)
        else:
            form = AjaxObjectiveForm(request.POST, audit=audit)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.audit = audit
            obj.changed_by = request.user
            obj.save()
            form.save_m2m()
            try:
                if papers:
                    Attachment.objects.bulk_create(
                        [Attachment(content_object=obj,
                                    workpaper=attachment, filename=attachment.name)
                         for attachment in papers])
            except:
                pass
            data['status'] = 'success'
            data['html'] = {}
            for process in obj.process.all().values_list('id', flat=True):
                data['html']['acc_process_%s' % process] = \
                    render_to_string('audits/ajax_binding_objective.html',
                                     {'audit_id': audit_id, 'obj': obj, 'process_id': process})
        else:
            data['error'] = form.errors
        return JsonResponse(data)


class AjaxObjectiveDeleteView(generic.View):
    @csrf_exempt
    def get(self, request, objective_id):
        data = {}
        get_object_or_404(Objective, id=objective_id).delete()
        data['status'] = 'success'
        return JsonResponse(data)


class AjaxRiskAddEditView(generic.View):
    @csrf_exempt
    def get(self, request, audit_id, risk_id=None):
        audit = get_object_or_404(Audit, id=audit_id)
        data = {}
        if risk_id:
            obj = get_object_or_404(Risk, id=risk_id)
            form = AjaxRiskForm(instance=obj, audit=audit)
        else:
            form = AjaxRiskForm(audit=audit)
        csrf_token_value = request.COOKIES['csrftoken']
        data['html'] = render_to_string('audits/right_panel_risk.html',
                                        {'form': form, 'audit_id': audit_id, 'risk_id': risk_id,
                                         'csrf_token_value': csrf_token_value})
        return JsonResponse(data)

    @csrf_exempt
    def post(self, request, audit_id, risk_id=None):
        data = {'status': 'failed'}
        audit = get_object_or_404(Audit, id=audit_id)
        papers = request.FILES.getlist('papers')
        if risk_id:
            obj = get_object_or_404(Risk, id=risk_id)
            form = AjaxRiskForm(request.POST, instance=obj, audit=audit)
        else:
            form = AjaxRiskForm(request.POST, audit=audit)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.audit = audit
            obj.changed_by = request.user
            obj.save()
            form.save_m2m()
            try:
                if papers:
                    Attachment.objects.bulk_create(
                        [Attachment(content_object=obj,
                                    workpaper=attachment, filename=attachment.name)
                         for attachment in papers])
            except:
                pass
            data['status'] = 'success'
            data['html'] = {}
            for objective in obj.objective.all():
                for process in objective.process.all() \
                        .values_list('id', flat=True):
                    data['html']['acc_pro_%s_obj_%s' % (process,
                                                        objective.id)] = \
                        render_to_string('audits/ajax_binding_risk.html',
                                         {'audit_id': audit_id, 'obj': obj,
                                          'process_id': process,
                                          'objective_id': objective.id})
        else:
            data['error'] = form.errors
        return JsonResponse(data)


class AjaxRiskDeleteView(generic.View):
    @csrf_exempt
    def get(self, request, risk_id):
        data = {}
        get_object_or_404(Risk, id=risk_id).delete()
        data['status'] = 'success'
        return JsonResponse(data)


class AjaxControlAddEditView(generic.View):
    @csrf_exempt
    def get(self, request, audit_id, control_id=None):
        audit = get_object_or_404(Audit, id=audit_id)
        data = {}
        if control_id:
            obj = get_object_or_404(Control, id=control_id)
            form = AjaxControlForm(instance=obj, audit=audit)
        else:
            form = AjaxControlForm(audit=audit)
        csrf_token_value = request.COOKIES['csrftoken']
        data['html'] = render_to_string('audits/right_panel_control.html',
                                        {'form': form, 'audit_id': audit_id, 'control_id': control_id,
                                         'csrf_token_value': csrf_token_value})
        return JsonResponse(data)

    @csrf_exempt
    def post(self, request, audit_id, control_id=None):
        data = {'status': 'failed'}
        audit = get_object_or_404(Audit, id=audit_id)
        papers = request.FILES.getlist('papers')
        if control_id:
            obj = get_object_or_404(Control, id=control_id)
            form = AjaxControlForm(request.POST, instance=obj, audit=audit)
        else:
            form = AjaxControlForm(request.POST, audit=audit)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.audit = audit
            obj.changed_by = request.user
            obj.save()
            form.save_m2m()
            try:
                if papers:
                    Attachment.objects.bulk_create(
                        [Attachment(content_object=obj,
                                    workpaper=attachment, filename=attachment.name)
                         for attachment in papers])
            except:
                pass
            data['status'] = 'success'
            data['html'] = {}
            for risk in obj.risk.all():
                for objective in risk.objective.all():
                    for process in objective.process.all() \
                            .values_list('id', flat=True):
                        data['html']['acc_pro_%s_obj_%s_risk_%s' % (process,
                                                                    objective.id, risk.id)] = \
                            render_to_string('audits/ajax_binding_control.html',
                                             {'audit_id': audit_id, 'obj': obj,
                                              'process_id': process, 'risk_id': risk.id,
                                              'objective_id': objective.id})
        else:
            data['error'] = form.errors
        return JsonResponse(data)


class AjaxControlDeleteView(generic.View):
    @csrf_exempt
    def get(self, request, control_id):
        data = {}
        get_object_or_404(Control, id=control_id).delete()
        data['status'] = 'success'
        return JsonResponse(data)


class AjaxTestAddEditView(generic.View):
    @csrf_exempt
    def get(self, request, audit_id, test_id=None):
        audit = get_object_or_404(Audit, id=audit_id)
        data = {}
        if test_id:
            obj = get_object_or_404(Test, id=test_id)
            form = AjaxTestForm(instance=obj, audit=audit)
        else:
            form = AjaxTestForm(audit=audit)
        csrf_token_value = request.COOKIES['csrftoken']
        data['html'] = render_to_string('audits/right_panel_test.html',
                                        {'form': form, 'audit_id': audit_id, 'test_id': test_id,
                                         'csrf_token_value': csrf_token_value})
        return JsonResponse(data)

    @csrf_exempt
    def post(self, request, audit_id, test_id=None):
        data = {'status': 'failed'}
        audit = get_object_or_404(Audit, id=audit_id)
        papers = request.FILES.getlist('papers')
        if test_id:
            obj = get_object_or_404(Test, id=test_id)
            form = AjaxTestForm(request.POST, instance=obj, audit=audit)
        else:
            form = AjaxTestForm(request.POST, audit=audit)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.audit = audit
            obj.changed_by = request.user
            obj.save()
            form.save_m2m()
            try:
                if papers:
                    Attachment.objects.bulk_create(
                        [Attachment(content_object=obj,
                                    workpaper=attachment, filename=attachment.name)
                         for attachment in papers])
            except:
                pass
            data['status'] = 'success'
            data['html'] = {}
            for control in obj.control.all():
                for risk in control.risk.all():
                    for objective in risk.objective.all():
                        for process in objective.process.all() \
                                .values_list('id', flat=True):
                            data['html']['acc_pro_%s_obj_%s_risk_%s_ctrl_%s' % (process,
                                                                                objective.id, risk.id, control.id)] = \
                                render_to_string('audits/ajax_binding_test.html',
                                                 {'audit_id': audit_id, 'obj': obj,
                                                  'process_id': process, 'risk_id': risk.id,
                                                  'objective_id': objective.id,
                                                  'control_id': control.id})
        else:
            data['error'] = form.errors
        return JsonResponse(data)


class AjaxTestDeleteView(generic.View):
    @csrf_exempt
    def get(self, request, test_id):
        data = {}
        get_object_or_404(Test, id=test_id).delete()
        data['status'] = 'success'
        return JsonResponse(data)


class AuditReport(generic.View):

    def get(self, request, pk):
        context = {}
        context['object'] = get_object_or_404(Audit, id=pk)
        context['test_passed_count'] = context['object'].test_audit.all() \
            .filter(result='PS').count()
        context['test_passed'] = context['object'].test_audit.all() \
            .filter(result='PS')
        context['test_failed'] = context['object'].test_audit.all() \
            .filter(result='FL')
        context['controls_passed'] = context['object'].control_audit.all() \
            .filter(test_control__result='PS').exclude(test_control__result='FL') \
            .distinct().count()

        return render(request, "audits/report_template.html", context)


class AuditCopyView(generic.View):
    @csrf_exempt
    def get(self, request, audit_id):
        data = {}
        audits = Audit.objects.exclude(id=audit_id)
        data = {'audits': audits}
        return render(request, 'audits/copy/audit_copy.html', data)

    def _save_proces(self, audit, cur_audit, processes):
        for process in processes:
            objectives = process.obj_process.all()
            process.audit = cur_audit
            process.id = None
            process.save()
            self._save_objectives(audit, cur_audit, process, objectives)

    def _save_objectives(self, audit, cur_audit, process, objectives):
        for objective in objectives:
            risks = objective.risk_obj.all()
            objective.audit = cur_audit
            objective.id = None
            objective.save()
            objective.process.add(process)
            self._save_risks(audit, cur_audit, objective, risks)

    def _save_risks(self, audit, cur_audit, objective, risks):
        for risk in risks:
            controls = risk.control_risk.all()
            risk.audit = cur_audit
            risk.id = None
            risk.save()
            risk.objective.add(objective)
            self._save_controls(audit, cur_audit, risk, controls)

    def _save_controls(self, audit, cur_audit, risk, controls):
        for control in controls:
            tests = control.test_control.all()
            control.id = None
            control.audit = cur_audit
            control.save()
            control.risk.add(risk)
            self._save_tests(audit, cur_audit, control, tests)

    def _save_tests(self, audit, cur_audit, control, tests):
        for test in tests:
            test.id = None
            test.audit = cur_audit
            test.save()
            test.control.add(control)

    def post(self, request, audit_id):
        # audit = request.POST['audit']
        # audit = Audit.objects.get(id=audit)
        cur_audit = Audit.objects.get(id=audit_id)
        src_dict = {'audit': Audit, 'process': Process, 'objective': Objective,
                    'risk': Risk, 'control': Control, 'test': Test}
        src = request.POST['copy_source']
        parents = request.POST['copy_parents'].split('|')
        if src in src_dict.keys():
            obj_id = int(request.POST['copy_object'])
            obj = get_object_or_404(src_dict[src], pk=obj_id)
            if src == 'audit':
                processes = obj.process_audit.all()
                self._save_proces(obj, cur_audit, processes)
            else:
                audit = obj.audit
                if src == 'process':
                    self._save_proces(audit, cur_audit, [obj])
                else:
                    process = get_object_or_404(Process, pk=int(parents[1]))
                    process.audit = cur_audit
                    process.id = None
                    process.save()
                    if src == 'objective':
                        self._save_objectives(audit, cur_audit,
                                              process, [obj])
                    else:
                        objective = get_object_or_404(Objective,
                                                      pk=int(parents[2]))
                        objective.audit = cur_audit
                        objective.id = None
                        objective.save()
                        objective.process.add(process)
                        if src == 'risk':
                            self._save_risks(audit, cur_audit,
                                             objective, [obj])
                        else:
                            risk = get_object_or_404(Risk,
                                                     pk=int(parents[3]))
                            risk.audit = cur_audit
                            risk.id = None
                            risk.save()
                            risk.objective.add(objective)
                            if src == 'control':
                                self._save_controls(audit, cur_audit,
                                                    risk, [obj])
                            else:
                                control = get_object_or_404(Control,
                                                            pk=int(parents[4]))
                                control.id = None
                                control.audit = cur_audit
                                control.save()
                                control.risk.add(risk)
                                self._save_tests(audit, cur_audit,
                                                 control, [obj])
        else:
            messages.error(request, gettext('Unable to copy from audit'))
            return HttpResponseRedirect(reverse('audits:index'))
        # processes = audit.process_audit.all()
        # self._save_proces(audit, cur_audit, processes)
        return HttpResponseRedirect(reverse('audits:index'))


class AuditCopyPushRepo(generic.View):
    @csrf_exempt
    def get(self, request, audit_id):
        data = {}
        audits = Audit.objects.filter(id=audit_id)
        data = {'audits': audits,
                'repos': Process_Group.objects.all().values('id', 'title')}
        return render(request, 'audits/copy/push_repo.html', data)

    def update_attachments(self, src_obj, dest_obj):
        RAttachment.objects.bulk_create(
            [RAttachment(content_object=dest_obj,
                         workpaper=File(attachment.workpaper, attachment.filename),
                         filename=attachment.filename)
             for attachment in src_obj.papers.all()])

    def _save_proces(self, pg, processes):
        for process in processes:
            objectives = process.obj_process.all()
            rp = RP.objects.create(title=process.title, owner=process.owner,
                                   ref_id=process.ref_id, desc=process.desc, changed_by=self.request.user, pg=pg)
            self.update_attachments(process, rp)
            self._save_objectives(rp, objectives, pg)

    def _save_objectives(self, process, objectives, pg):
        for objective in objectives:
            risks = objective.risk_obj.all()
            ro = RO.objects.create(title=objective.title, pg=pg,
                                   ref_id=objective.ref_id, desc=objective.desc, changed_by=self.request.user)
            ro.process.add(process)
            self.update_attachments(objective, ro)
            self._save_risks(ro, risks, pg)

    def _save_risks(self, objective, risks, pg):
        for risk in risks:
            controls = risk.control_risk.all()
            rr = RR.objects.create(title=risk.title, ref_id=risk.ref_id, desc=risk.desc,
                                   changed_by=self.request.user, treatment=risk.treatment, pg=pg)
            rr.objective.add(objective)
            self.update_attachments(risk, rr)
            self._save_controls(rr, controls, pg)

    def _save_controls(self, risk, controls, pg):
        for control in controls:
            tests = control.test_control.all()
            cr = RC.objects.create(title=control.title,
                                   ref_id=control.ref_id, desc=control.desc, changed_by=self.request.user,
                                   control_type=control.control_type,
                                   control_class=control.control_class,
                                   frequency=control.frequency, owner=control.owner, pg=pg)
            cr.risk.add(risk)
            self.update_attachments(control, cr)
            self._save_tests(cr, tests, pg)

    def _save_tests(self, control, tests, pg):
        for test in tests:
            rt = RT.objects.create(title=test.title, ref_id=test.ref_id,
                                   desc=test.desc, changed_by=self.request.user, pg=pg)
            self.update_attachments(test, rt)
            rt.control.add(control)

    def post(self, request, audit_id):
        if request.POST['process_group'] == 'new':
            pg_name = request.POST['pg_name']
            pg = Process_Group.objects.create(title=pg_name, desc='',
                                              changed_by=request.user)
        else:
            pg = Process_Group.objects.get(id=request.POST['pg'])
        src_dict = {'audit': Audit, 'process': Process, 'objective': Objective,
                    'risk': Risk, 'control': Control, 'test': Test}
        src = request.POST['copy_source']
        parents = request.POST['copy_parents'].split('|')
        if src in src_dict:
            obj_id = int(request.POST['copy_object'])
            obj = get_object_or_404(src_dict[src], pk=obj_id)
            if src == 'audit':
                processes = obj.process_audit.all()
                self._save_proces(pg, processes)
            else:
                if src == 'process':
                    self._save_proces(pg, [obj])
                else:
                    process = get_object_or_404(Process, pk=int(parents[1]))
                    rp = RP.objects.create(title=process.title, pg=pg, ref_id=process.ref_id,
                                           desc=process.desc, changed_by=self.request.user)
                    self.update_attachments(process, rp)
                    if src == 'objective':
                        self._save_objectives(rp, [obj], pg)
                    else:
                        objective = get_object_or_404(Objective,
                                                      pk=int(parents[2]))
                        ro = RO.objects.create(title=objective.title, pg=pg, ref_id=objective.ref_id,
                                               desc=objective.desc, changed_by=self.request.user)
                        ro.process.add(rp)
                        self.update_attachments(objective, ro)
                        if src == 'risk':
                            self._save_risks(ro, [obj], pg)
                        else:
                            risk = get_object_or_404(Risk,
                                                     pk=int(parents[3]))
                            rr = RR.objects.create(title=risk.title, pg=pg, ref_id=risk.ref_id,
                                                   desc=risk.desc, changed_by=self.request.user,
                                                   treatment=risk.treatment)
                            rr.objective.add(ro)
                            self.update_attachments(risk, rr)
                            if src == 'control':
                                self._save_controls(rr, [obj], pg)
                            else:
                                control = get_object_or_404(Control,
                                                            pk=int(parents[4]))
                                cr = RC.objects.create(title=control.title, ref_id=control.ref_id,
                                                       desc=control.desc,
                                                       changed_by=self.request.user,
                                                       control_type=control.control_type,
                                                       control_class=control.control_class,
                                                       frequency=control.frequency,
                                                       owner=control.owner, pg=pg)
                                cr.risk.add(rr)
                                self.update_attachments(control, cr)
                                self._save_tests(cr, [obj], pg)

            messages.success(request, gettext('Pushed selected audit objects into Repository'))
        else:
            messages.error(request, gettext('Unable to push audit to Repository'))
            return HttpResponseRedirect(reverse('audits:audit_push_repo', kwargs={'audit_id': audit_id}))
        return HttpResponseRedirect(reverse('audits:audit_push_repo', kwargs={'audit_id': audit_id}))


class RepoCopy(generic.View):

    def update_attachments(self, src_obj, dest_obj):
        Attachment.objects.bulk_create(
            [Attachment(content_object=dest_obj,
                        workpaper=File(attachment.workpaper, attachment.filename),
                        filename=attachment.filename)
             for attachment in src_obj.papers.all()])

    def _save_process_group(self, audit, pg):
        if pg.repo_process_Group_belong_to.all().exists():
            for pg_obj in pg.repo_process_Group_belong_to.all():
                self._save_process_group(audit, pg_obj)
        elif pg.process_group.all().exists():
            self._save_process(audit, pg.process_group.all())

    def _save_process(self, cur_audit, processes):
        for process in processes:
            objectives = process.obj_process.all()
            a_process = Process.objects.create(audit=cur_audit,
                                               title=process.title, ref_id=process.ref_id,
                                               desc=process.desc, changed_by=self.request.user)
            self.update_attachments(process, a_process)
            self._save_objectives(cur_audit, a_process, objectives)

    def _save_objectives(self, cur_audit, process, objectives):
        for objective in objectives:
            risks = objective.risk_obj.all()
            a_objective = Objective.objects.create(title=objective.title, ref_id=objective.ref_id,
                                                   desc=objective.desc, audit=cur_audit, changed_by=self.request.user)
            a_objective.process.add(process)
            self.update_attachments(objective, a_objective)
            self._save_risks(cur_audit, a_objective, risks)

    def _save_risks(self, cur_audit, objective, risks):
        for risk in risks:
            controls = risk.control_risk.all()
            a_risk = Risk.objects.create(title=risk.title, ref_id=risk.ref_id, desc=risk.desc,
                                         audit=cur_audit, changed_by=self.request.user,
                                         treatment=risk.treatment)
            a_risk.objective.add(objective)
            self.update_attachments(risk, a_risk)
            self._save_controls(cur_audit, a_risk, controls)

    def _save_controls(self, cur_audit, risk, controls):
        for control in controls:
            tests = control.test_control.all()
            a_control = Control.objects.create(title=control.title, ref_id=control.ref_id,
                                               desc=control.desc, audit=cur_audit,
                                               changed_by=self.request.user,
                                               control_type=control.control_type,
                                               control_class=control.control_class,
                                               frequency=control.frequency)
            a_control.risk.add(risk)
            self.update_attachments(control, a_control)
            self._save_tests(cur_audit, a_control, tests)

    def _save_tests(self, cur_audit, control, tests):
        for test in tests:
            a_test = Test.objects.create(title=test.title, ref_id=test.ref_id,
                                         desc=test.desc, audit=cur_audit, changed_by=self.request.user)
            a_test.control.add(control)
            self.update_attachments(test, a_test)

    def post(self, request, audit_id):
        audit = Audit.objects.get(id=audit_id)
        src_dict = {'process_group': Process_Group, 'process': RP,
                    'objective': RO,
                    'risk': RR, 'control': RC, 'test': RT}
        src = request.POST['copy_source']
        # parents = request.POST['copy_parents'].split('|')
        selected_list = request.POST['selected_id'].split('_')
        if src in src_dict:
            obj_id = int(request.POST['copy_object'])
            obj = get_object_or_404(src_dict[src], pk=obj_id)
            if src == 'process_group':
                self._save_process_group(audit, obj)
            elif src == 'process':
                self._save_process(audit, [obj])
            else:
                idx = int(selected_list.index(src)) + 1
                if src == 'objective':
                    rp = get_object_or_404(RP, id=int(selected_list[idx - 2]))
                    process = Process.objects.create(title=rp.title,
                                                     ref_id=rp.ref_id, desc=rp.desc,
                                                     audit=audit, changed_by=request.user)
                    self.update_attachments(rp, process)
                    self._save_objectives(audit, process, [obj])
                elif src == 'risk':
                    rp = get_object_or_404(RP, id=int(selected_list[idx - 4]))
                    process = Process.objects.create(title=rp.title,
                                                     ref_id=rp.ref_id, desc=rp.desc,
                                                     audit=audit, changed_by=request.user)
                    self.update_attachments(rp, process)
                    ro = get_object_or_404(RO, id=int(selected_list[idx - 2]))
                    objective = Objective.objects.create(title=ro.title,
                                                         ref_id=ro.ref_id, desc=ro.desc, audit=audit, changed_by=request.user)
                    objective.process.add(process)
                    self.update_attachments(ro, objective)
                    self._save_risks(audit, objective, [obj])
                elif src == 'control':
                    rp = get_object_or_404(RP, id=int(selected_list[idx - 6]))
                    process = Process.objects.create(title=rp.title,
                                                     ref_id=rp.ref_id, desc=rp.desc,
                                                     audit=audit, changed_by=request.user)
                    self.update_attachments(rp, process)
                    ro = get_object_or_404(RO, id=int(selected_list[idx - 4]))
                    objective = Objective.objects.create(title=ro.title,
                                                         ref_id=ro.ref_id, desc=ro.desc, audit=audit, changed_by=request.user)
                    objective.process.add(process)
                    self.update_attachments(ro, objective)
                    rr = get_object_or_404(RR, id=int(selected_list[idx - 2]))
                    risk = Risk.objects.create(title=rr.title, ref_id=rr.ref_id, desc=rr.desc,
                                               audit=audit, changed_by=request.user,
                                               treatment=rr.treatment)
                    risk.objective.add(objective)
                    self.update_attachments(rr, risk)
                    self._save_controls(audit, risk, [obj])
                elif src == 'test':
                    rp = get_object_or_404(RP, id=int(selected_list[idx - 8]))
                    process = Process.objects.create(title=rp.title,
                                                     ref_id=rp.ref_id, desc=rp.desc, audit=audit, changed_by=request.user)
                    self.update_attachments(rp, process)
                    ro = get_object_or_404(RO, id=int(selected_list[idx - 6]))
                    objective = Objective.objects.create(title=ro.title,
                                                         ref_id=ro.ref_id, desc=ro.desc, audit=audit, changed_by=request.user)
                    objective.process.add(process)
                    self.update_attachments(ro, objective)
                    rr = get_object_or_404(RR, id=int(selected_list[idx - 4]))
                    risk = Risk.objects.create(title=rr.title, ref_id=rr.ref_id, desc=rr.desc,
                                               audit=audit, changed_by=request.user,
                                               treatment=rr.treatment)
                    self.update_attachments(rr, risk)
                    risk.objective.add(objective)
                    rc = get_object_or_404(RC, id=int(selected_list[idx - 2]))
                    control = Control.objects.create(title=rc.title,
                                                     ref_id=rc.ref_id, desc=rc.desc,
                                                     audit=audit, changed_by=request.user,
                                                     control_type=rc.control_type,
                                                     control_class=rc.control_class,
                                                     frequency=rc.frequency)
                    control.risk.add(risk)
                    self.update_attachments(rc, control)
                    self._save_tests(audit, control, [obj])
            messages.success(request, gettext('Copied selected audit objects from Repository'))
        else:
            messages.error(request, gettext('Unable to copy from Repository'))
            return HttpResponseRedirect(reverse('audits:repo_copy_view', kwargs={'audit_id': audit_id}))
        return HttpResponseRedirect(reverse('audits:audit_management', kwargs={'audit_id': audit_id}))
