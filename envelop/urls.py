"""envelop URL Configuration

"""
from django.conf import settings
# from django.conf.urls import include, urls
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import path, include
from django.views.generic import TemplateView
from django.views.static import serve

from audits.views import index, LoginView

admin.site.site_header = 'Envelop administration'
admin.AdminSite.site_title = 'Envelop'

urlpatterns = [
    path('', login_required(index), name='index'),
    path('login/', LoginView.as_view(), name='envelope_login'),
    path('not_implemented/', TemplateView.as_view(template_name="not_implemented.html")),
    path('audits/', include(('audits.urls', 'audits'), namespace="audits")),
    path('admin/', admin.site.urls),
    #    url(r'^files/', include('db_file_storage.urls')),
    path('planning/', include(('planning.urls', 'planning'), namespace='planning')),
    # url('help/', serve, {'document_root': settings.DOCS_ROOT, 'path': 'index.html'}),
    # url('help/(?P<path>.*)$', serve, {'document_root': settings.DOCS_ROOT}),
    path('repository/', include('repository.urls', "repo")),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
