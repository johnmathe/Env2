function UpdateSelectMultiple() {
    $('select[multiple]').multiselect({
        enableFiltering: true,
        filterBehavior: 'text',
        buttonWidth: '400px',
        dropRight: true,
        maxHeight: 200
    });
}